//
//  AlertView.swift
//  Food
//
//  Created by Abdul Muqeem on 12/10/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class AlertView: UIView {

    @IBOutlet weak var lblMessage:UILabel!
    
    @IBOutlet weak var btnOkAction:UIButton!
    @IBOutlet weak var btnCancel:UIButton!
    
    var view: UIView!
    var delegateAction:AlertViewDelegateAction?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    
    func xibSetup(){
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
    }
    
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    @IBAction func okButtonAction(_ sender : UIButton) {
        self.delegateAction?.okButtonAction()
    }
    
    @IBAction func cancelAction(_ sender : UIButton) {
        self.delegateAction?.cancelAction()
    }
    
}
