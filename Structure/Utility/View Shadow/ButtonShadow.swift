//
//  ButtonShadow.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 02/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func viewDropShadow(view : UIView) {
        view.layer.cornerRadius = view.frame.height * 0.45
        view.layer.shadowColor = UIColor.init(red: 255.0/255.0, green: 237/255.0, blue: 220.0/255.0, alpha: 1.0).cgColor
        view.layer.shadowOffset = CGSize(width: 0,height: 20) //CGSize(width: 0, height: 30)
        view.layer.shadowOpacity = 0.6
        view.layer.shadowRadius = 4.0
    }
    
    func buttonDropShadow(button : UIButton) {
        button.layer.shadowColor = UIColor.init(red: 255.0/255.0, green: 237/255.0, blue: 220.0/255.0, alpha: 1.0).cgColor
        button.layer.shadowOffset = CGSize(width: 0,height: 20) //CGSize(width: 0, height: 30)
        button.layer.shadowOpacity = 0.6
        button.layer.shadowRadius = 4.0
    }
    
    func viewShadow(view : UIView , radius : CGFloat) {
        view.layer.cornerRadius = view.frame.height * radius
        view.layer.shadowColor = UIColor.gray.cgColor//UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0).cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowOpacity = 0.6
        view.layer.shadowRadius = 3.0
    }
    
    func buttonShadow(button : UIButton , radius : CGFloat) {
        button.layer.cornerRadius = button.frame.height * radius
        button.layer.shadowColor = UIColor.gray.cgColor//UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0).cgColor
        button.layer.shadowOffset = CGSize(width: 3, height: 3)
        button.layer.shadowOpacity = 0.6
        button.layer.shadowRadius = 3.0
    }
    
    func viewShadowInfo(view : UIView , radius : CGFloat) {
        view.layer.cornerRadius = view.frame.height * radius
        view.layer.shadowColor = UIColor.gray.cgColor//UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0).cgColor
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.layer.shadowOpacity = 0.6
        view.layer.shadowRadius = 1.0
    }
    
}
