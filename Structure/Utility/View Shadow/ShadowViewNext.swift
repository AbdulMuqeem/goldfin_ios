//
//  ShadowViewNext.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 7/25/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import UIKit

class ShadowView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        // corner radius
        self.layer.cornerRadius = self.frame.height/2
        self.layoutIfNeeded()

        // border
//        self.layer.borderWidth = 1.0
//        self.layer.borderColor = UIColor.black.cgColor

        // shadow
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 1.0
    }

}
