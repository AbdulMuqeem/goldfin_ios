//
//  bulletViews.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 7/25/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func bulletViews(view : UIView) {
        view.layoutIfNeeded()
        view.layer.cornerRadius = view.frame.height/2
    }
    
}
