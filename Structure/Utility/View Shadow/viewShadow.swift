//
//  viewShadow.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/11/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func addShadow(view : UIView) {
        view.layoutIfNeeded()
        view.layer.cornerRadius = view.frame.height * 0.1
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 1
    }
    
    func addShadowCorner(view : UIView) {
        view.layer.borderWidth = 2.0
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.shadowColor = UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0).cgColor
        view.layer.shadowOpacity = 1.0
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowRadius = 3
    }
    
}

extension UIView {

    func addshadow(top: Bool,
               left: Bool,
               bottom: Bool,
               right: Bool,
               shadowRadius: CGFloat = 1.0) {

    self.layer.masksToBounds = false
    self.layer.shadowOffset = CGSize.zero
    self.layer.shadowRadius = shadowRadius
    self.layer.shadowOpacity = 0.4

    let path = UIBezierPath()
    var x: CGFloat = 0
    var y: CGFloat = 0
    var viewWidth = self.frame.width * 0.91
    var viewHeight = self.frame.height * 0.91

    // here x, y, viewWidth, and viewHeight can be changed in
    // order to play around with the shadow paths.
    if (!top) {
        y+=(shadowRadius+1)
    }
    if (!bottom) {
        viewHeight-=(shadowRadius+1)
    }
    if (!left) {
        x+=(shadowRadius+1)
    }
    if (!right) {
        viewWidth-=(shadowRadius+1)
    }
    // selecting top most point
    path.move(to: CGPoint(x: x, y: y))
    // Move to the Bottom Left Corner, this will cover left edges
    /*
     |☐
     */
    path.addLine(to: CGPoint(x: x, y: viewHeight))
    // Move to the Bottom Right Corner, this will cover bottom edge
    /*
     ☐
     -
     */
    path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
    // Move to the Top Right Corner, this will cover right edge
    /*
     ☐|
     */
    path.addLine(to: CGPoint(x: viewWidth, y: y))
    // Move back to the initial point, this will cover the top edge
    /*
     _
     ☐
     */
    path.close()
    self.layer.shadowPath = path.cgPath
}
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

    func addShadow(cornerRadius: CGFloat, maskedCorners: CACornerMask, color: UIColor, offset: CGSize, opacity: Float, shadowRadius: CGFloat) {
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = cornerRadius
        self.layer.maskedCorners = maskedCorners
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = shadowRadius
    }
    
    func addShadowOnly(cornerRadius: CGFloat, maskedCorners: CACornerMask, color: UIColor, offset: CGSize, opacity: Float, shadowRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.maskedCorners = maskedCorners
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = shadowRadius
    }
    
}
