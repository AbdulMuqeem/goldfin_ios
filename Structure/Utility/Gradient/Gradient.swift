//
//  Gradient.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/11/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import UIKit

class ActualGradientButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [ UIColor.init(red: 255.0/255.0, green: 213.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 246.0/255.0, green: 217.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor]//[UIColor.systemYellow.cgColor, UIColor.systemPink.cgColor]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.cornerRadius = l.frame.height * 0.5
        layer.insertSublayer(l, at: 0)
        return l
    }()
}

class ActualGradientView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [ UIColor.init(red: 255.0/255.0, green: 213.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 246.0/255.0, green: 217.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor]//[UIColor.systemYellow.cgColor, UIColor.systemPink.cgColor]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.cornerRadius = l.frame.height * 0.5
        layer.insertSublayer(l, at: 0)
        return l
    }()
}

class ActualGradientViewPlain: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [ UIColor.init(red: 255.0/255.0, green: 213.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 246.0/255.0, green: 217.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor]//[UIColor.systemYellow.cgColor, UIColor.systemPink.cgColor]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        l.cornerRadius = l.frame.height * 0.5
        layer.insertSublayer(l, at: 0)
        return l
    }()
}

class ActualGradientGrayViewPlain: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [ UIColor.lightGray.cgColor, UIColor.lightGray.cgColor]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        l.cornerRadius = l.frame.height * 0.5
        layer.insertSublayer(l, at: 0)
        return l
    }()
}
