//
//  attributedStrings.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 7/25/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func setAttributedString(mainString : String, coloredString : String) {
        let main_string = mainString //"GoldFin Loan Calculator"
        let string_to_color = coloredString //"Calculator"
        let range = (main_string as NSString).range(of: string_to_color)
        let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 141.0/255.0, green: 141.0/255.0, blue: 141.0/255.0, alpha: 1.0) , range: range)
        self.attributedText = attribute
    }
}

extension UIButton {

    func underline() {
        guard let title = self.titleLabel else { return }
        guard let tittleText = title.text else { return }
        let attributedString = NSMutableAttributedString(string: (tittleText))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: (tittleText.count)))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
}

extension UILabel {
    
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
    
}
