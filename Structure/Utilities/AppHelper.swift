//
//  AppHelper.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import Alamofire

class AppHelper: NSObject {
    
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
        
    static func isValidEmail(testStr:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: testStr)
    }
    
    static func isValidPersonalNumber(str:String) -> Bool {
        let regex = "^[0-9]*$"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: str)
    }
    
    static func isValidURL(str:String) -> Bool {
        let regex = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: str)
    }
    
    static func isValidDocumentNumber(regex:String,str:String) -> Bool {
        let regex = regex
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: str)
    }

    static public func isValidPassword(testStr:String) -> Bool {
        let passwordRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).*"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
    }
    
    static func isValidAge(testStr:String) -> Bool {
        if testStr == "0" {
            return false
        }
        if Int.init(testStr)! > 150 {
            return false
        }
        if testStr.count > 3 {
            return false
        }
        return true
    }
    
    static func isValidPhone(testStr:String , minPhone:Int , maxPhone:Int) -> Bool {
        if testStr.count < minPhone {
            return false
        } else if testStr.count > maxPhone {
            return false
        }
        return true
    }
    
    static func isValidName(testStr:String , minPhone:Int , maxPhone:Int) -> Bool {
        if testStr.count < minPhone {
            return false
        } else if testStr.count > maxPhone {
            return false
        }
        return true
    }
    
}
