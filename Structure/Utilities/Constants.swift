//
//  Constants.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

public let THEME_COLOR: UInt = 0xFFDC59

public let BASE_URL = "http://134.209.144.186:3000/" // Development

let APP_STORE_ID = "itms://itunes.apple.com/app/id1516461913"

public let LOGO_IMAGE: UIImage = UIImage(named:"logo")!
public let BACK_IMAGE: UIImage = UIImage(named:"back")!

// Alert
public let SUCCESS_IMAGE: UIImage = UIImage(named:"success")!
public let FAILURE_IMAGE: UIImage = UIImage(named:"error")!

public let WARNING:UIColor = UIColor.init(named: "Warning_Color")!
public let FAILURE:UIColor = UIColor.init(named: "Failure_Color")!
public let SUCCESS:UIColor = UIColor.init(named: "Success_Color")!

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

public var FORM_STATUS:FormStatus?

public var SIGNUP:Bool? = false
public var AMOUNT:String? = ""
public var TOLA:String? = ""
public var GRAM:String? = ""

// Extra

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"


