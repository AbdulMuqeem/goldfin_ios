//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSObject , NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "_id"
        static let phoneVerification = "phoneVerification"
        static let phone = "phone"
        static let accountStatus = "accountStatus"
        static let fullname = "fullname"
        static let cnic = "cnic"
        static let email = "email"
        static let type = "type"
        static let v = "__v"
        static let createdAt = "createdAt"
        static let lafform = "lafform"
        static let formDone = "formDone"
        static let userName = "userName"
        static let token = "token"
        static let planVisitData = "planVisitData"
    }
    
    // MARK: Properties
    public var id: String?
    public var phoneVerification: String?
    public var phone: String?
    public var accountStatus: String?
    public var fullname: String?
    public var cnic: String?
    public var email: String?
    public var type: String?
    public var v: String?
    public var createdAt: String?
    public var lafform: String?
    public var formDone: String?
    public var userName: String?
    public var token: String?
    public var planVisitData: PlanVisitData?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].string
        phoneVerification = json[SerializationKeys.phoneVerification].string
        phone = json[SerializationKeys.phone].string
        accountStatus = json[SerializationKeys.accountStatus].string
        fullname = json[SerializationKeys.fullname].string
        cnic = json[SerializationKeys.cnic].string
        email = json[SerializationKeys.email].string
        type = json[SerializationKeys.type].string
        v = json[SerializationKeys.v].string
        createdAt = json[SerializationKeys.createdAt].string
        lafform = json[SerializationKeys.lafform].string
        formDone = json[SerializationKeys.formDone].string
        userName = json[SerializationKeys.userName].string
        token = json[SerializationKeys.token].string
        planVisitData = PlanVisitData(json: json[SerializationKeys.planVisitData])
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = phoneVerification { dictionary[SerializationKeys.phoneVerification] = value }
        if let value = phone { dictionary[SerializationKeys.phone] = value }
        if let value = accountStatus { dictionary[SerializationKeys.accountStatus] = value }
        if let value = fullname { dictionary[SerializationKeys.fullname] = value }
        if let value = cnic { dictionary[SerializationKeys.cnic] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = type { dictionary[SerializationKeys.type] = value }
        if let value = v { dictionary[SerializationKeys.v] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = lafform { dictionary[SerializationKeys.lafform] = value }
        if let value = formDone { dictionary[SerializationKeys.formDone] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = token { dictionary[SerializationKeys.token] = value }
        if let value = planVisitData { dictionary[SerializationKeys.planVisitData] = value.dictionaryRepresentation() }
        
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
        self.phoneVerification = aDecoder.decodeObject(forKey: SerializationKeys.phoneVerification) as? String
        self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
        self.accountStatus = aDecoder.decodeObject(forKey: SerializationKeys.accountStatus) as? String
        self.fullname = aDecoder.decodeObject(forKey: SerializationKeys.fullname) as? String
        self.cnic = aDecoder.decodeObject(forKey: SerializationKeys.cnic) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.type = aDecoder.decodeObject(forKey: SerializationKeys.type) as? String
        self.v = aDecoder.decodeObject(forKey: SerializationKeys.v) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.lafform = aDecoder.decodeObject(forKey: SerializationKeys.lafform) as? String
        self.formDone = aDecoder.decodeObject(forKey: SerializationKeys.formDone) as? String
        self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
        self.token = aDecoder.decodeObject(forKey: SerializationKeys.token) as? String
        self.planVisitData = aDecoder.decodeObject(forKey: SerializationKeys.planVisitData) as? PlanVisitData
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(phoneVerification, forKey: SerializationKeys.phoneVerification)
        aCoder.encode(phone, forKey: SerializationKeys.phone)
        aCoder.encode(accountStatus, forKey: SerializationKeys.accountStatus)
        aCoder.encode(fullname, forKey: SerializationKeys.fullname)
        aCoder.encode(cnic, forKey: SerializationKeys.cnic)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(type, forKey: SerializationKeys.type)
        aCoder.encode(v, forKey: SerializationKeys.v)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(lafform, forKey: SerializationKeys.lafform)
        aCoder.encode(formDone, forKey: SerializationKeys.formDone)
        aCoder.encode(userName, forKey: SerializationKeys.userName)
        aCoder.encode(token, forKey: SerializationKeys.token)
        aCoder.encode(planVisitData, forKey: SerializationKeys.planVisitData)
    }
    
}
