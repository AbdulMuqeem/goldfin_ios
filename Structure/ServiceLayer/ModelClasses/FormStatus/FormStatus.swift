//
//  FormStatus.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 08/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FormStatus :  NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let personalInformation = "personalInformation"
        static let professionalInformation = "professionalInformation"
        static let financialInformation = "financialInformation"
        static let generalInformation = "generalInformation"
        static let documentsUploading = "documentsUploading"
    }
    
    // MARK: Properties
    public var personalInformation: String?
    public var professionalInformation: String?
    public var financialInformation: String?
    public var generalInformation: String?
    public var documentsUploading: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        personalInformation = json[SerializationKeys.personalInformation].string
        professionalInformation = json[SerializationKeys.professionalInformation].string
        financialInformation = json[SerializationKeys.financialInformation].string
        generalInformation = json[SerializationKeys.generalInformation].string
        documentsUploading = json[SerializationKeys.documentsUploading].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = personalInformation { dictionary[SerializationKeys.personalInformation] = value }
        if let value = professionalInformation { dictionary[SerializationKeys.professionalInformation] = value }
        if let value = financialInformation { dictionary[SerializationKeys.financialInformation] = value }
        if let value = generalInformation { dictionary[SerializationKeys.generalInformation] = value }
        if let value = documentsUploading { dictionary[SerializationKeys.documentsUploading] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.personalInformation = aDecoder.decodeObject(forKey: SerializationKeys.personalInformation) as? String
        self.professionalInformation = aDecoder.decodeObject(forKey: SerializationKeys.professionalInformation) as? String
        self.financialInformation = aDecoder.decodeObject(forKey: SerializationKeys.financialInformation) as? String
        self.generalInformation = aDecoder.decodeObject(forKey: SerializationKeys.generalInformation) as? String
        self.documentsUploading = aDecoder.decodeObject(forKey: SerializationKeys.documentsUploading) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(personalInformation, forKey: SerializationKeys.personalInformation)
        aCoder.encode(professionalInformation, forKey: SerializationKeys.professionalInformation)
        aCoder.encode(financialInformation, forKey: SerializationKeys.financialInformation)
        aCoder.encode(generalInformation, forKey: SerializationKeys.generalInformation)
        aCoder.encode(documentsUploading, forKey: SerializationKeys.documentsUploading)
    }
    
}
