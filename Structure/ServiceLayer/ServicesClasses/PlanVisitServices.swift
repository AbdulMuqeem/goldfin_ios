//
//  PlanVisitServices.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 07/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import Foundation
import SwiftyJSON

class PlanVisitServices {
    
    static func VisitPlan(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.plan , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func CheckFormStatus(completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.formStatus + id , callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func CheckLoanStatus(completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.loanStatus + id , callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func SalariedPersonalInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.salariedPersonal , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func SalariedProfessionalInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.salariedProfessional , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func SalariedFinancialInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.salariedFinancial , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func BusinessPersonalInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.businessPersonal , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func BusinessProfessionalInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.businessProfessional , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }

    static func BusinessFinancialInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.businessFinancial , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GeneralInformation(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.invite , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["status"].int != 200 {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
}
