//
//  ServiceApiEndPoints.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 22/03/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation

class ServiceApiEndPoints: NSObject  {
    
    // Authentication
    static let login = BASE_URL + "auth/login"
    static let signup = BASE_URL + "auth/signup"
    static let sendOTP = BASE_URL + "auth/verifyAccount"
    static let reSendOTP = BASE_URL + "auth/signup"
    
    
    // Loan Calculator
    static let amount = BASE_URL + "rates/goldratemoney/"
    static let tola = BASE_URL + "rates/goldrateqty/tola/"
    static let gram = BASE_URL + "rates/goldrateqty/gram/"
    
    // Visit Plan
    static let plan = BASE_URL + "planVisit/addNewVisit"
    
    // Status
    static let formStatus = BASE_URL + "salariedLaf/getUserForms/"
    static let loanStatus = BASE_URL + "user/getLoanStatus/"
    
    //Salaried
    static let salariedPersonal = BASE_URL + "salariedLaf/lafForm1"
    static let salariedProfessional = BASE_URL + "salariedLaf/lafForm2"
    static let salariedFinancial = BASE_URL + "salariedLaf/lafForm3"
    static let salariedUploadDocuments = BASE_URL + "salariedLaf/addDocuments"
    
    //Business
    static let businessPersonal = BASE_URL + "businessLaf/lafForm1"
    static let businessProfessional = BASE_URL + "businessLaf/lafForm2"
    static let businessFinancial = BASE_URL + "businessLaf/lafForm3"
    static let businessUploadDocuments = BASE_URL + "businessLaf/addDocuments"
    
    static let invite = BASE_URL + "recommend/addToInvite"
    
}
