//
//  AppDelegate.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 20/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyBnz1Z14WDDlz95wcl2Kd2goy3ssUEkw3E")
        
        IQKeyboardManager.shared.enable = true
        self.navigateTOInitialViewController()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func navigateTOInitialViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let nav = RootViewController.instantiateFromStoryboard()
        self.window?.rootViewController = nav
        let vc = WelcomeViewController.instantiateFromStoryboard()
        nav.pushViewController(vc, animated: true)
        
    }
    
}

