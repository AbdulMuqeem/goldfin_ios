//
//  SceneDelegate.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 20/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var backgroundUpdateTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        self.navigateTOInitialViewController()
        
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        
    }
    
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    static func getInstatnce() -> SceneDelegate {
        let scene = UIApplication.shared.connectedScenes.first
        return scene?.delegate as! SceneDelegate
    }
    
    func navigateTOInitialViewController() {
        
        let nav = RootViewController.instantiateFromStoryboard()
        self.window?.rootViewController = nav
        let vc = WelcomeViewController.instantiateFromStoryboard()
        nav.pushViewController(vc, animated: true)
        
    }
    
}

