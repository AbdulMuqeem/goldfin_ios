//
//  ThankYouVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/11/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension ThankYouViewController {
    
    func initialSetup(){
        buttonDropShadow(button: buttonPressHere)
    }
    
}

class ThankYouViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ThankYouViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ThankYouViewController
    }
    
    @IBOutlet weak var buttonPressHere: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    @IBAction func actionPressHere(_ sender: UIButton) {
        SIGNUP = true
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: RegisterViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

}


