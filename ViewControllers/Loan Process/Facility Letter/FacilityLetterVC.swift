//
//  FacilityLetterVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/16/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class FacilityLetterVC: UIViewController {

    
    @IBOutlet weak var viewNext: UIButton!
    @IBOutlet weak var viewIBAN: UIView!
    @IBOutlet weak var viewPer: UIView!
    @IBOutlet weak var viewMark: UIView!
    @IBOutlet weak var viewDisbursed: UIView!
    @IBOutlet weak var viewCIN: UIView!
    @IBOutlet weak var viewCNIC: UIView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewLoan: UIView!
    @IBOutlet weak var Facility: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    
    }
    

    @IBAction func actionNext(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "ThankYouLoanVC") as! ThankYouLoanVC //HomeVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FacilityLetterVC
{
    func initialSetup()
    {
        viewLoan.layoutIfNeeded()
        viewShadowInfo(view: viewLoan, radius: 0.15)
        viewType.layoutIfNeeded()
        viewShadowInfo(view: viewType, radius: 0.15)
        viewName.layoutIfNeeded()
        viewShadowInfo(view: viewName, radius: 0.15)
        viewStatus.layoutIfNeeded()
        viewShadowInfo(view: viewStatus, radius: 0.15)
        viewCNIC.layoutIfNeeded()
        viewShadowInfo(view: viewCNIC, radius: 0.15)
        viewCIN.layoutIfNeeded()
        viewShadowInfo(view: viewCIN, radius: 0.15)
        viewDisbursed.layoutIfNeeded()
        viewShadowInfo(view: viewDisbursed, radius: 0.15)
        viewMark.layoutIfNeeded()
        viewShadowInfo(view: viewMark, radius: 0.15)
        viewPer.layoutIfNeeded()
        viewShadowInfo(view: viewPer, radius: 0.15)
        viewIBAN.layoutIfNeeded()
        viewShadowInfo(view: viewIBAN, radius: 0.15)
        
        buttonDropShadow(button: viewNext)
        Facility.setAttributedString(mainString: "Facility Letter", coloredString: "Letter")
    }
}
