//
//  ProfessionalInformationVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/14/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class SalariedProfessionalInformationVC: UIViewController {
    
    class func instantiateFromStoryboard() -> SalariedProfessionalInformationVC {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SalariedProfessionalInformationVC
    }
    
    
    @IBOutlet weak var Professional: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var txtFieldEmployerLandline: UITextField!
    @IBOutlet weak var viewInnerEmployerLandline: UIView!
    @IBOutlet weak var viewEmployerLandline: UIView!
    @IBOutlet weak var txtFieldEmployerWebsite: UITextField!
    @IBOutlet weak var viewInnerEmployerWebsite: UIView!
    @IBOutlet weak var viewEmployerWebsite: UIView!
    @IBOutlet weak var txtFieldEmployerPhone: UITextField!
    @IBOutlet weak var viewInnerEmployerPhone: UIView!
    @IBOutlet weak var viewEmployerPhone: UIView!
    @IBOutlet weak var txtFieldEmployerAddress: UITextField!
    @IBOutlet weak var viewInnerEmployerAddress: UIView!
    @IBOutlet weak var viewEmployerAddress: UIView!
    @IBOutlet weak var txtFieldEmployerName: UITextField!
    @IBOutlet weak var viewInnerEmployerName: UIView!
    @IBOutlet weak var viewEmployerName: UIView!
    @IBOutlet weak var txtFieldWorkingExperience: UITextField!
    @IBOutlet weak var labelDateOfJoining: UILabel!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var labelResidence: UILabel!
    @IBOutlet weak var viewResidence: UIView!
    @IBOutlet weak var txtFieldDesignation: UITextField!
    @IBOutlet weak var viewInnerDesignation: UIView!
    @IBOutlet weak var viewDesignation: UIView!
    @IBOutlet weak var txtFieldProfession: UITextField!
    @IBOutlet weak var viewProfessional: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.txtFieldProfession.text = "Salaried"
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSelectProfession(_ sender: UIButton) {
        //showPickerView(tagPicker: 7)
    }
    
    @IBAction func actionSelectResidence(_ sender: UIButton) {
        showPickerView(tagPicker: 8)
    }
    
    @IBAction func actionDateOfJoining(_ sender: UIButton) {
        let popOverVC = UIStoryboard(name: "LoanProcess", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        popOverVC.delegate = self
        popOverVC.isDateFormat = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }

    @IBAction func actionNext(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let profession = self.txtFieldProfession.text!
        let designation = self.txtFieldDesignation.text!
        var status = self.labelResidence.text!
        let joiningDate = self.labelDateOfJoining.text!
        let experience = self.txtFieldWorkingExperience.text!
        let name = self.txtFieldEmployerName.text!
        let address = self.txtFieldEmployerAddress.text!
        let phone = self.txtFieldEmployerPhone.text!
        let website = self.txtFieldEmployerWebsite.text!
        let landline = self.txtFieldEmployerLandline.text!
        
        if status == "Job Status?" {
            status = ""
        }
                
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "profession":profession , "designation":designation , "jobstatus":status , "dateofjoining":joiningDate , "totalexp":experience , "empname":name , "empaddress": address , "phone":phone , "website":website , "landline":landline]
        print(dataDict)
        
        PlanVisitServices.SalariedProfessionalInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                
                self.showBanner(title: "Success !!!", subTitle: "Your professional information form is submitted successfully" , type: SUCCESS)
                
                let vc = SalariedFinancialInfoVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        })
        
    }
    
    
}

extension SalariedProfessionalInformationVC {
    
    func initialSetup() {
        viewDate.layoutIfNeeded()
        viewShadowInfo(view: viewDate, radius: 0.15)
        viewProfessional.layoutIfNeeded()
        viewShadowInfo(view: viewProfessional, radius: 0.15)
        viewResidence.layoutIfNeeded()
        viewShadowInfo(view: viewResidence, radius: 0.15)
        viewDesignation.layoutIfNeeded()
        viewShadowInfo(view: viewDesignation, radius: 0.15)
        viewInnerDesignation.addShadowOnly(cornerRadius: viewInnerDesignation.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewEmployerName.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerName, radius: 0.15)
        viewInnerEmployerName.addShadowOnly(cornerRadius: viewInnerEmployerName.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewEmployerAddress.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerAddress, radius: 0.15)
        viewInnerEmployerAddress.addShadowOnly(cornerRadius: viewInnerEmployerAddress.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewEmployerPhone.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerPhone, radius: 0.15)
        viewInnerEmployerPhone.addShadowOnly(cornerRadius: viewInnerEmployerPhone.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewEmployerWebsite.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerWebsite, radius: 0.15)
        viewInnerEmployerWebsite.addShadowOnly(cornerRadius: viewInnerEmployerWebsite.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        
        viewEmployerLandline.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerLandline, radius: 0.15)
        viewInnerEmployerLandline.addShadowOnly(cornerRadius: viewInnerEmployerLandline.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        
        buttonDropShadow(button: buttonNext)
        Professional.setAttributedString(mainString: "Professional Information", coloredString: "Information")
    }
    
    func showPickerView(tagPicker : Int) {
        let popOverVC = UIStoryboard(name: "PopUp", bundle: nil).instantiateViewController(withIdentifier: "PurposeOfLoanPopUp") as! PurposeOfLoanPopUp
        popOverVC.delegateProfession = self
        popOverVC.delegateJobStatus = self
        popOverVC.pickerTag = tagPicker
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }
    
}

extension SalariedProfessionalInformationVC : selectProfessionDelegate {
    func selectProfession(Profession: String) {
        txtFieldProfession.text = Profession
    }
}

extension SalariedProfessionalInformationVC : jobStatusDelegate {
    func selectJobStatus(status: String) {
        labelResidence.text = status
    }
}

extension SalariedProfessionalInformationVC : DateAndTimePickerDelegate {
    
    func sendTimeAndDate(DateAndTime: String) {
        print(DateAndTime)
        labelDateOfJoining.text = DateAndTime
    }
    
}

