//
//  ProfessionalInfoVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class ProfessionalInfoVC: UIViewController {

    class func instantiateFromStoryboard() -> ProfessionalInfoVC {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfessionalInfoVC
    }
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewInnerTotal: UIView!
    @IBOutlet weak var viewTotalWorkingExperience: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewInnerJobStatus: UIView!
    @IBOutlet weak var viewJobStatus: UIView!
    @IBOutlet weak var viewInnerWebsite: UIView!
    @IBOutlet weak var viewWebsite: UIView!
    @IBOutlet weak var viewInnerPhone: UIView!
    @IBOutlet weak var viewPhoen: UIView!
    @IBOutlet weak var viewInnerEmployerAddress: UIView!
    @IBOutlet weak var viewEmployerAddress: UIView!
    @IBOutlet weak var viewInnerEmployername: UIView!
    @IBOutlet weak var viewEmployerName: UIView!
    @IBOutlet weak var viewInnerDesignation: UIView!
    @IBOutlet weak var viewDesignation: UIView!
    @IBOutlet weak var Professional: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionNext(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
//        let nextVC = storyboard.instantiateViewController(withIdentifier: "FinancialInfoVC") as! FinancialInfoVC //HomeVC
//        self.navigationController?.pushViewController(nextVC, animated: true)
    }

}

extension ProfessionalInfoVC {
    
    func initialSetup() {
        viewDesignation.layoutIfNeeded()
        viewShadowInfo(view: viewDesignation, radius: 0.15)
        viewInnerDesignation.addShadowOnly(cornerRadius: viewInnerDesignation.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewEmployerName.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerName, radius: 0.15)
        viewInnerEmployername.addShadowOnly(cornerRadius: viewInnerEmployername.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewEmployerAddress.layoutIfNeeded()
        viewShadowInfo(view: viewEmployerAddress, radius: 0.15)
        viewInnerEmployerAddress.addShadowOnly(cornerRadius: viewInnerEmployerAddress.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewPhoen.layoutIfNeeded()
        viewShadowInfo(view: viewPhoen, radius: 0.15)
        viewInnerPhone.addShadowOnly(cornerRadius: viewInnerPhone.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewWebsite.layoutIfNeeded()
        viewShadowInfo(view: viewWebsite, radius: 0.15)
        viewInnerWebsite.addShadowOnly(cornerRadius: viewInnerWebsite.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewJobStatus.layoutIfNeeded()
        viewShadowInfo(view: viewJobStatus, radius: 0.15)
        viewInnerJobStatus.addShadowOnly(cornerRadius: viewInnerJobStatus.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewDate.layoutIfNeeded()
        viewShadowInfo(view: viewDate, radius: 0.1)
        viewTotalWorkingExperience.layoutIfNeeded()
        viewShadowInfo(view: viewTotalWorkingExperience, radius: 0.15)
        viewInnerTotal.addShadowOnly(cornerRadius: viewInnerTotal.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        buttonDropShadow(button: buttonNext)
        Professional.setAttributedString(mainString: "Professional Information", coloredString: "Information")
    }
}
