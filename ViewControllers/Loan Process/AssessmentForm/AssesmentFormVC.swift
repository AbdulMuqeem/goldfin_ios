//
//  AssesmentFormVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class AssesmentFormVC: UIViewController {

    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewInnerShroff: UIView!
    @IBOutlet weak var viewInnerEvaluating: UIView!
    @IBOutlet weak var viewShroff: UIView!
    @IBOutlet weak var viewEvaluating: UIView!
    @IBOutlet weak var For: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    
    @IBAction func actionNext(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "VerificationFormVC") as! VerificationFormVC //HomeVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }

}

extension AssesmentFormVC {
    
    func initialSetup() {
        viewEvaluating.layoutIfNeeded()
        viewShadowInfo(view: viewEvaluating, radius: 0.15)
        viewInnerEvaluating.addShadowOnly(cornerRadius: viewInnerEvaluating.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewShroff.layoutIfNeeded()
        viewShadowInfo(view: viewShroff, radius: 0.15)
        viewInnerShroff.addShadowOnly(cornerRadius: viewInnerShroff.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewDate.layoutIfNeeded()
        viewShadowInfo(view: viewDate, radius: 0.1)
        buttonDropShadow(button: buttonNext)
        For.setAttributedString(mainString: "For Collateral Management Officer (CMO)", coloredString: "(CMO)")
    }
    
}
