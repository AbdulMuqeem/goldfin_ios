//
//  DatePickerVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/14/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

protocol DateAndTimePickerDelegate {
    func sendTimeAndDate(DateAndTime : String)
}

class DatePickerVC: UIViewController {
    
    var isFromPersonalInfo:Bool! = false
    
    var isDateFormat = false
    var delegate : DateAndTimePickerDelegate!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isDateFormat == true {
            datePicker.datePickerMode = .date
        }
        if isDateFormat == false {
            datePicker.datePickerMode = .time
        }
        
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        if isDateFormat == true {
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let inputDate = dateFormatter.string(from: datePicker.date)
//            let currentDate = Date().to_Date_String()
//            if self.isFromPersonalInfo == true {
//                if inputDate < currentDate {
                    delegate.sendTimeAndDate(DateAndTime: inputDate)
//                }
//                else {
//                    self.showBanner(title: "Alert", subTitle: "Please select valid birth date", type: FAILURE)
//                }
//            }
//            else {
//                if inputDate > currentDate {
//                    delegate.sendTimeAndDate(DateAndTime: inputDate)
//                }
//                else {
//                    self.showBanner(title: "Alert", subTitle: "Please select upcoming date", type: FAILURE)
//                }
//            }
            
        }
        else {
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.timeZone = TimeZone.autoupdatingCurrent
            dateFormatter.timeStyle = DateFormatter.Style.short
            let date = self.datePicker.date
            let strTime = date.dateStringWith(strFormat: "hh:mm a")
            delegate.sendTimeAndDate(DateAndTime: strTime)
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension Date {
    
    func dateStringWith(strFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        dateFormatter.dateFormat = strFormat
        return dateFormatter.string(from: self)
    }
    
}
