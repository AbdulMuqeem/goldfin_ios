//
//  PlanVisitViewController.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/14/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class PlanVisitViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> PlanVisitViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PlanVisitViewController
    }
    
    var isDateSelected = true
    var DatePickerMode = true
    
    @IBOutlet weak var viewTimeAndDate: UIView!
    @IBOutlet weak var selectTimeAndDate: UIButton!
    @IBOutlet weak var buttonSelectTime: UIButton!
    @IBOutlet weak var buttonSelectDate: UIButton!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        if let value = Singleton.sharedInstance.CurrentUser!.planVisitData {
            if let date = value.date {
                self.labelDate.text = date
            }
            else {
                let date = self.getCurrentDate()
                self.labelDate.text = date
            }
            
            if let time = value.time {
                self.labelTime.text = time
            }
            else {
                let time = self.getCurrentTime()
                self.labelTime.text = time
            }
        }
        else {
            
            let date = self.getCurrentDate()
            self.labelDate.text = date
            
            let time = self.getCurrentTime()
            self.labelTime.text = time
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
        
    }
    
    @IBAction func actionSelectTime(_ sender: UIButton) {
        if Singleton.sharedInstance.CurrentUser?.planVisitData!.time == self.labelTime.text {
            self.ShowAlert(title: "Alert", message: "Are you sure you want to update selected time") { (sender) in
                self.isDateSelected = false
                self.showTimePicker()
                return
            }
        }
        self.isDateSelected = false
        self.showTimePicker()
    }
    
    @IBAction func actionSelectDate(_ sender: UIButton) {
        if Singleton.sharedInstance.CurrentUser?.planVisitData!.time == self.labelTime.text {
            self.ShowAlert(title: "Alert", message: "Are you sure you want to update selected date") { (sender) in
                self.isDateSelected = true
                self.showDatePicker()
                return
            }
        }
        self.isDateSelected = true
        self.showDatePicker()
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if Singleton.sharedInstance.CurrentUser?.planVisitData!.time == self.labelTime.text && Singleton.sharedInstance.CurrentUser?.planVisitData!.date == self.labelDate.text  {
            let vc = EmployementTypeViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let date = self.labelDate.text!
        let time = self.labelTime.text!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        if date.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please select date", type: FAILURE)
            return
        }
        
        if time.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please select time", type: FAILURE)
            return
        }
        
//        if date < self.getCurrentDate() {
//            self.showBanner(title: "Alert", subTitle: "Please select valid date", type: FAILURE)
//            return
//        }
//
//        if time < self.getCurrentTime() {
//            if date < self.getCurrentDate() || date == self.getCurrentDate() {
//                self.showBanner(title: "Alert", subTitle: "Please select valid time", type: FAILURE)
//                return
//            }
//        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "date":date , "time":time]
        print(dataDict)
        
        PlanVisitServices.VisitPlan(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                
                if let msg = response["Message"].string {
                    self.showBanner(title: "Success !!!", subTitle: msg , type: SUCCESS)
                }
                else {
                    self.showBanner(title: "Success !!!", subTitle: "Added date & time for your visit plan" , type: SUCCESS)
                }
                
                let vc = EmployementTypeViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        })
    }
    
}

extension PlanVisitViewController {
    
    func initialSetup() {
        dateView.layoutIfNeeded()
        viewShadow(view: dateView, radius: 0.45)
        timeView.layoutIfNeeded()
        viewShadow(view: timeView, radius: 0.45)
    }
    
    func showDatePicker() {
        let popOverVC = UIStoryboard(name: "LoanProcess", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        popOverVC.delegate = self
        popOverVC.isDateFormat = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }
    
    func showTimePicker() {
        let popOverVC = UIStoryboard(name: "LoanProcess", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        popOverVC.delegate = self
        popOverVC.isDateFormat = false
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }
    
}

extension PlanVisitViewController : DateAndTimePickerDelegate {
    
    func sendTimeAndDate(DateAndTime: String) {
        print(DateAndTime)
        
        if self.isDateSelected == true {
            self.labelDate.text = DateAndTime
            
        }
        else if self.isDateSelected == false {
            self.labelTime.text = DateAndTime
        }
        
    }
}

extension PlanVisitViewController {
    
    func selectButton(button : UIButton ,label : UILabel) {
        button.backgroundColor = UIColor.init(red: 251.0/255.0, green: 214.0/255.0, blue: 41.0/255.0, alpha: 1.0)
        label.isHidden = false
    }
    
    func deSelectButton(button : UIButton ,label : UILabel) {
        button.backgroundColor = UIColor.init(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        label.isHidden = true
    }
    
}
