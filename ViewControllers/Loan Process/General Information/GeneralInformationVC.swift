//
//  GeneralInformationVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit
import ContactsUI

class GeneralInformationVC: UIViewController , CNContactPickerDelegate {
    
    class func instantiateFromStoryboard() -> GeneralInformationVC {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! GeneralInformationVC
    }
    
    @IBOutlet weak var viewContact: UIView!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var General: UILabel!
    
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblFirstNumber: UILabel!
    
    @IBOutlet weak var lblSecondName: UILabel!
    @IBOutlet weak var lblSecondNumber: UILabel!
    
    @IBOutlet weak var lblThirdName: UILabel!
    @IBOutlet weak var lblThirdNumber: UILabel!
    
    @IBOutlet weak var lblFourthName: UILabel!
    @IBOutlet weak var lblFourthNumber: UILabel!
    
    @IBOutlet weak var lblFifthName: UILabel!
    @IBOutlet weak var lblFifthNumber: UILabel!
    
    var nameArray:[String] = [String]()
    var numberArray:[String] = [String]()
    
    var isFromBusiness:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        viewContact.layer.borderWidth = 1
        viewContact.layer.borderColor = UIColor.init(red: 252.0/255.0, green: 190.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contactAction(_ sender : UIButton) {
        
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            self.presentSettingsAlert()
            return
        }
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                self.presentSettingsAlert()
                return
            }
            
            // get the contacts
            
            let cnPicker = CNContactPickerViewController()
            cnPicker.delegate = self
            self.present(cnPicker, animated: true, completion: nil)
            
        }
        
    }
    
    func presentSettingsAlert() {
        
        self.ShowAlert(title: "Access Permission", message: "Goldfin App needs to access your contacts to show your contacts list") { (sender) in
            
            let url = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
    
    //MARK:- CNContactPickerDelegate Method
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        
        let contactDetails = contactProperty.contact
        
        let name = contactDetails.givenName
        
        if self.nameArray.count == 0 {
            
            self.lblFirstName.text = name
            self.nameArray.append(name)
            
            if let identifier = contactProperty.identifier {
                
                let number = contactDetails.phoneNumbers
                
                for a in number {
                    
                    if identifier == a.identifier {
                        let result = a.value.stringValue.filter("01234567890".contains)
                        self.lblFirstNumber.text = result
                        self.numberArray.append(result)
                        return
                    }
                }
            }
        }
        else if self.nameArray.count == 1 {
            
            self.lblSecondName.text = name
            self.nameArray.append(name)
            
            if let identifier = contactProperty.identifier {
                
                let number = contactDetails.phoneNumbers
                
                for a in number {
                    
                    if identifier == a.identifier {
                        let result = a.value.stringValue.filter("01234567890".contains)
                        self.lblSecondNumber.text = result
                        self.numberArray.append(result)
                        return
                    }
                }
            }
        }
        else if self.nameArray.count == 2 {
            
            self.lblThirdName.text = name
            self.nameArray.append(name)
            
            if let identifier = contactProperty.identifier {
                
                let number = contactDetails.phoneNumbers
                
                for a in number {
                    
                    if identifier == a.identifier {
                        let result = a.value.stringValue.filter("01234567890".contains)
                        self.lblThirdNumber.text = result
                        self.numberArray.append(result)
                        return
                    }
                }
            }
        }
        else if self.nameArray.count == 3 {
            
            self.lblFourthName.text = name
            self.nameArray.append(name)
            
            if let identifier = contactProperty.identifier {
                
                let number = contactDetails.phoneNumbers
                
                for a in number {
                    
                    if identifier == a.identifier {
                        let result = a.value.stringValue.filter("01234567890".contains)
                        self.lblFourthNumber.text = result
                        self.numberArray.append(result)
                        return
                    }
                }
            }
        }
        else if self.nameArray.count == 4 {
            
            self.lblFifthName.text = name
            self.nameArray.append(name)
            
            if let identifier = contactProperty.identifier {
                
                let number = contactDetails.phoneNumbers
                
                for a in number {
                    
                    if identifier == a.identifier {
                        let result = a.value.stringValue.filter("01234567890".contains)
                        self.lblFirstNumber.text = result
                        self.numberArray.append(result)
                        return
                    }
                }
            }
        }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "name":self.nameArray , "phone":self.numberArray]
        print(dataDict)
        
        PlanVisitServices.GeneralInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                
                self.showBanner(title: "Success !!!", subTitle: "Your general information form is submitted successfully" , type: SUCCESS)
                
                if self.isFromBusiness != true {
                    let vc = DocumentUploadVC.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    let vc = BusinessDocumentUploadViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        })
        
    }
}

extension GeneralInformationVC {
    
    func initialSetup() {
        buttonDropShadow(button: buttonNext)
        General.setAttributedString(mainString: "General Information", coloredString: "Information")
    }
    
}
