//
//  LoanStatusVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/16/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class LoanStatusViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoanStatusViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoanStatusViewController
    }
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewTo: UIView!
    @IBOutlet weak var viewDue: UIView!
    @IBOutlet weak var viewProcessing: UIView!
    @IBOutlet weak var viewMarkUp: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewLoan: UIView!
    @IBOutlet weak var loan: UILabel!
    
    @IBOutlet weak var backButton:UIView!
    var isFromSignIn:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        if self.isFromSignIn == true {
            self.backButton.isHidden = true
            //self.buttonNext.isHidden = true
        }
        
        self.CheckStatus()
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func CheckStatus() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        PlanVisitServices.CheckLoanStatus(completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                if let title = response["loanStatus"].string {
                    if let message = response["message"].string {
                        self.Alert(title: title , message: message)
                    }
                }
            }
        })
        
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "ThankYouLoanVC") as! ThankYouLoanVC //HomeVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}

extension LoanStatusViewController {
    
    func initialSetup() {
        viewLoan.layoutIfNeeded()
        viewShadowInfo(view: viewLoan, radius: 0.15)
        viewMobile.layoutIfNeeded()
        viewShadowInfo(view: viewMobile, radius: 0.15)
        viewMarkUp.layoutIfNeeded()
        viewShadowInfo(view: viewMarkUp, radius: 0.15)
        viewProcessing.layoutIfNeeded()
        viewShadowInfo(view: viewProcessing, radius: 0.15)
        viewDue.layoutIfNeeded()
        viewShadowInfo(view: viewDue, radius: 0.15)
        viewTo.layoutIfNeeded()
        viewShadowInfo(view: viewTo, radius: 0.15)
        buttonDropShadow(button: buttonNext)
        loan.setAttributedString(mainString: "Loan Status", coloredString: "Status")
    }
    
}
