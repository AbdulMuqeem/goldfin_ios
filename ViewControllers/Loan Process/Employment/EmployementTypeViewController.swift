//
//  EmployementVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/12/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class EmployementTypeViewController : UIViewController {
    
    class func instantiateFromStoryboard() -> EmployementTypeViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EmployementTypeViewController
    }
    
    @IBOutlet weak var viewSalaried: UIView!
    @IBOutlet weak var viewBussiness: UIView!
    
    @IBOutlet weak var viewEnterprise: UIView!
    @IBOutlet weak var viewAgriculture: UIView!
        
    @IBOutlet weak var Loan: UILabel!
    var isHide = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewAgriculture.isHidden = true
        self.viewEnterprise.isHidden = true
        self.initialSetup()

    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        let vc = InfoTypeViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionSalaried(_ sender: UIButton) {
        let vc = InfoTypeViewController.instantiateFromStoryboard()
        vc.employementType = "Salaried"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionBusiness(_ sender: UIButton) {
        if self.isHide == true {
            self.viewAgriculture.isHidden = false
            self.viewEnterprise.isHidden = false
            self.isHide = false
        }
        else {
            self.viewAgriculture.isHidden = true
            self.viewEnterprise.isHidden = true
            self.isHide = true
        }
    }
    
    @IBAction func actionAgriculture(_ sender: UIButton) {
        let vc = InfoTypeViewController.instantiateFromStoryboard()
        vc.employementType = "Agriculture"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionEnterprise(_ sender: UIButton) {
        let vc = InfoTypeViewController.instantiateFromStoryboard()
        vc.employementType = "Enterprise"
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
}

extension EmployementTypeViewController {
    
    func initialSetup() {
        viewBussiness.addShadow(cornerRadius: viewBussiness.frame.height * 0.45, maskedCorners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner], color: UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0), offset: CGSize(width: 2, height: 2), opacity: 0.5, shadowRadius: 2.0)
        viewEnterprise.addShadow(cornerRadius: viewEnterprise.frame.height * 0.45, maskedCorners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner], color: UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0), offset: CGSize(width: 2, height: 2), opacity: 0.5, shadowRadius: 2.0)
        viewSalaried.addShadow(cornerRadius: viewSalaried.frame.height * 0.45, maskedCorners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner], color: UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0), offset: CGSize(width: 2, height: 2), opacity: 0.5, shadowRadius: 4.0)
        viewAgriculture.addShadow(cornerRadius: viewAgriculture.frame.height * 0.45, maskedCorners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner], color: UIColor.init(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0), offset: CGSize(width: 2, height: 2), opacity: 0.5, shadowRadius: 4.0)
    }
    
}
