//
//  VerificationFormVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class VerificationFormVC: UIViewController {

    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewReason: UIView!
    @IBOutlet weak var viewBusiness: UIView!
    @IBOutlet weak var viewResidance: UIView!
    @IBOutlet weak var viewFinancial: UIView!
    @IBOutlet weak var viewProfessional: UIView!
    @IBOutlet weak var viewPersonal: UIView!
    @IBOutlet weak var Verification: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "LoanDetailsVC") as! LoanDetailsVC //HomeVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension VerificationFormVC
{
    func initialSetup()
    {
        viewPersonal.layoutIfNeeded()
        viewShadowInfo(view: viewPersonal, radius: 0.15)
        viewProfessional.layoutIfNeeded()
        viewShadowInfo(view: viewProfessional, radius: 0.15)
        viewFinancial.layoutIfNeeded()
        viewShadowInfo(view: viewFinancial, radius: 0.15)
        viewResidance.layoutIfNeeded()
        viewShadowInfo(view: viewResidance, radius: 0.15)
        viewBusiness.layoutIfNeeded()
        viewShadowInfo(view: viewBusiness, radius: 0.15)
        viewReason.layoutIfNeeded()
        viewShadowInfo(view: viewReason, radius: 0.15)
        buttonDropShadow(button: buttonNext)
        Verification.setAttributedString(mainString: "Verification Form", coloredString: "Form")
    }
}
