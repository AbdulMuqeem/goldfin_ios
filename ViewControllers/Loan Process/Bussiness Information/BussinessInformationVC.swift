//
//  BussinessInformationVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension BussinessInformationVC : DateAndTimePickerDelegate {
    
    func sendTimeAndDate(DateAndTime: String) {
        print(DateAndTime)
        self.txtFieldRegisterSince.text = DateAndTime
    }
    
}


class BussinessInformationVC: UIViewController {

    class func instantiateFromStoryboard() -> BussinessInformationVC {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! BussinessInformationVC
    }
    
    @IBOutlet weak var viewInnerAnimal: UIView!
    @IBOutlet weak var viewAnimal: UIView!
    @IBOutlet weak var viewInnerMainCrops: UIView!
    @IBOutlet weak var viewMainCrops: UIView!
    @IBOutlet weak var viewInnerCultivating: UIView!
    @IBOutlet weak var viewCultivating: UIView!
    @IBOutlet weak var viewInnerRantedLand: UIView!
    @IBOutlet weak var viewRentedLAnd: UIView!
    @IBOutlet weak var viewInnerOwnLand: UIView!
    @IBOutlet weak var viewOwnLand: UIView!
    @IBOutlet weak var viewInnerWorking: UIView!
    @IBOutlet weak var viewWorking: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewInnerCompanyName: UIView!
    @IBOutlet weak var viewCompanyName: UIView!
    @IBOutlet weak var txtFieldNatureOfBusiness: UITextField!
    @IBOutlet weak var txtFieldCompanyName: UITextField!
    @IBOutlet weak var txtFieldSecp: UITextField!
    @IBOutlet weak var txtFieldRegisterSince: UILabel!
    @IBOutlet weak var txtFieldNTN: UITextField!
    @IBOutlet weak var txtFieldNumberOfEmployes: UITextField!
    @IBOutlet weak var txtFieldWorking: UITextField!
    @IBOutlet weak var txtFieldOwnLand: UITextField!
    @IBOutlet weak var txtFieldRentLand: UITextField!
    @IBOutlet weak var txtFieldCultivating: UITextField!
    @IBOutlet weak var txtFieldMainCrops: UITextField!
    @IBOutlet weak var txtFieldAnimals: UITextField!
    @IBOutlet weak var viewInnerNatureOfBusiness: UIView!
    @IBOutlet weak var viewNatureOfBusiness: UIView!
    @IBOutlet weak var txtFieldStatus: UITextField!
    @IBOutlet weak var viewInnerStatus: UIView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var txtFieldBusinessWebsite: UITextField!
    @IBOutlet weak var viewInnerBusinessWebsite: UIView!
    @IBOutlet weak var viewBusinessWebsite: UIView!
    @IBOutlet weak var txtFieldLandline: UITextField!
    @IBOutlet weak var viewInnerBusinessLandline: UIView!
    @IBOutlet weak var viewBusinessLandline: UIView!
    @IBOutlet weak var txtFieldBusinessAddress: UITextField!
    @IBOutlet weak var viewInnerBusinessAddress: UIView!
    @IBOutlet weak var viewBusinessAddress: UIView!
    @IBOutlet weak var labelBusinessStatus: UITextField!
    @IBOutlet weak var viewInnerBusinessStatus: UIView!
    @IBOutlet weak var viewBusinessStatus: UIView!
    @IBOutlet weak var labelBusinessTitle: UITextField!
    @IBOutlet weak var viewInnerTitle: UIView!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var Bussiness: UILabel!
    
    @IBOutlet weak var buttonNext: UIButton!
    
    var isDateSelected = true
    var DatePickerMode = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        self.txtFieldRegisterSince.text = self.getCurrentDate()
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func dateAction(_ sender:UIButton) {
        self.isDateSelected = true
        self.showDatePicker()
    }
    
    func showDatePicker() {
        let popOverVC = UIStoryboard(name: "LoanProcess", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        popOverVC.delegate = self
        popOverVC.isDateFormat = true
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let bussinessname = self.labelBusinessTitle.text!
        let businessStatus = self.labelBusinessStatus.text!
        let bussinessaddress = self.txtFieldBusinessAddress.text!
        let bussinesslandline = self.txtFieldLandline.text!
        let bussinesswebsite = self.txtFieldBusinessWebsite.text!
        let bussinessOwnstatus = self.txtFieldStatus.text!
        let natureofbusiness = self.txtFieldNatureOfBusiness.text!
        let companyname = self.txtFieldCompanyName.text!
        let secp = self.txtFieldSecp.text!
        let dateofreg = self.txtFieldRegisterSince.text!
        let ntn = self.txtFieldNTN.text!
        let nbrofemp = self.txtFieldNumberOfEmployes.text!
        let working = self.txtFieldWorking.text!
        let ownland = self.txtFieldOwnLand.text!
        let rentedland = self.txtFieldRentLand.text!
        let cultivating = self.txtFieldCultivating.text!
        let maincrops = self.txtFieldMainCrops.text!
        let animals = self.txtFieldAnimals.text!
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "bussinessname":bussinessname , "businessStatus":businessStatus , "bussinessaddress":bussinessaddress , "bussinesslandline":bussinesslandline , "bussinesswebsite":bussinesswebsite , "bussinessOwnstatus":bussinessOwnstatus , "natureofbusiness":natureofbusiness , "companyname":companyname , "secp":secp , "dateofreg":dateofreg , "ntn":ntn , "nbrofemp":nbrofemp , "working":working , "ownland":ownland , "rentedland":rentedland , "cultivating":cultivating , "maincrops":maincrops , "animals":animals]
        print(dataDict)
        
        PlanVisitServices.BusinessProfessionalInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            self.showBanner(title: "Success !!!", subTitle: "Your business information form is submitted successfully" , type: SUCCESS)
            
            let vc = FinancialInformationViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        })

    }

}

extension BussinessInformationVC {
    
    func initialSetup() {
        viewTitle.layoutIfNeeded()
        viewShadowInfo(view: viewTitle, radius: 0.15)
        viewInnerTitle.addShadowOnly(cornerRadius: viewInnerTitle.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewBusinessStatus.layoutIfNeeded()
        viewShadowInfo(view: viewBusinessStatus, radius: 0.15)
        viewInnerBusinessStatus.addShadowOnly(cornerRadius: viewInnerBusinessStatus.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewBusinessAddress.layoutIfNeeded()
        viewShadowInfo(view: viewBusinessAddress, radius: 0.15)
        viewInnerBusinessAddress.addShadowOnly(cornerRadius: viewInnerBusinessAddress.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewBusinessLandline.layoutIfNeeded()
        viewShadowInfo(view: viewBusinessLandline, radius: 0.15)
        viewInnerBusinessLandline.addShadowOnly(cornerRadius: viewInnerBusinessLandline.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewBusinessWebsite.layoutIfNeeded()
        viewShadowInfo(view: viewBusinessWebsite, radius: 0.15)
        viewInnerBusinessWebsite.addShadowOnly(cornerRadius: viewInnerBusinessWebsite.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewStatus.layoutIfNeeded()
        viewShadowInfo(view: viewStatus, radius: 0.15)
        viewInnerStatus.addShadowOnly(cornerRadius: viewInnerStatus.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewNatureOfBusiness.layoutIfNeeded()
        viewShadowInfo(view: viewNatureOfBusiness, radius: 0.15)
        viewInnerNatureOfBusiness.addShadowOnly(cornerRadius: viewInnerNatureOfBusiness.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewCompanyName.layoutIfNeeded()
        viewShadowInfo(view: viewCompanyName, radius: 0.15)
        viewInnerCompanyName.addShadowOnly(cornerRadius: viewInnerCompanyName.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewDate.layoutIfNeeded()
        viewShadowInfo(view: viewDate, radius: 0.1)
        
        viewWorking.layoutIfNeeded()
        viewShadowInfo(view: viewWorking, radius: 0.15)
        viewInnerWorking.addShadowOnly(cornerRadius: viewInnerWorking.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewOwnLand.layoutIfNeeded()
        viewShadowInfo(view: viewOwnLand, radius: 0.15)
        viewInnerOwnLand.addShadowOnly(cornerRadius: viewInnerOwnLand.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewRentedLAnd.layoutIfNeeded()
        viewShadowInfo(view: viewRentedLAnd, radius: 0.15)
        viewInnerRantedLand.addShadowOnly(cornerRadius: viewInnerRantedLand.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewCultivating.layoutIfNeeded()
        viewShadowInfo(view: viewCultivating, radius: 0.15)
        viewInnerCultivating.addShadowOnly(cornerRadius: viewInnerCultivating.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewMainCrops.layoutIfNeeded()
        viewShadowInfo(view: viewMainCrops, radius: 0.15)
        viewInnerMainCrops.addShadowOnly(cornerRadius: viewInnerMainCrops.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewAnimal.layoutIfNeeded()
        viewShadowInfo(view: viewAnimal, radius: 0.15)
        viewInnerAnimal.addShadowOnly(cornerRadius: viewInnerAnimal.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        Bussiness.setAttributedString(mainString: "Business Information", coloredString: "Information")
        buttonDropShadow(button: buttonNext)
    }
}
