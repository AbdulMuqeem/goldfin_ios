//
//  ThankYouLoanVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/16/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class ThankYouLoanVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func googleAction(_ sender : UIButton) {
        if let url = URL(string: "https://www.google.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }

    @IBAction func facebookAction(_ sender : UIButton) {
        if let url = URL(string: "https://www.facebook.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func twitterAction(_ sender : UIButton) {
        if let url = URL(string: "https://twitter.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func linkedinAction(_ sender : UIButton) {
        if let url = URL(string: "https://www.linkedin.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func finishAction(_ sender : UIButton) {
        self.ShowAlert(title: "Alert", message: "Do you want to close this application?") { (sender) in
            exit(0)
        }
    }
    
}
