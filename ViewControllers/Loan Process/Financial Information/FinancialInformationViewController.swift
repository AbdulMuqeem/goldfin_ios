//
//  FinancialInformationVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class FinancialInformationViewController : UIViewController {

    class func instantiateFromStoryboard() -> FinancialInformationViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FinancialInformationViewController
    }
    
    @IBOutlet weak var Financial: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewTotalExpenses: UIView!
    @IBOutlet weak var viewMisc: UIView!
    @IBOutlet weak var viewCommittee: UIView!
    @IBOutlet weak var viewChildrenEducation: UIView!
    @IBOutlet weak var viewHouseRent: UIView!
    @IBOutlet weak var viewExpenseDetails: UIView!
    @IBOutlet weak var viewAverageMonthly: UIView!
    @IBOutlet weak var viewInnerLoanAmount: UIView!
    @IBOutlet weak var viewLoanAmount: UIView!
    @IBOutlet weak var viewInnerSourceLoan: UIView!
    @IBOutlet weak var viewSourceLoan: UIView!
    
    @IBOutlet weak var txtFieldMonthlyTurnOver: UITextField!
    @IBOutlet weak var txtFieldMonthlyBusinessExpense: UITextField!
    @IBOutlet weak var txtFieldOtherIncome: UITextField!
    @IBOutlet weak var txtFieldSourceOtherIncome: UITextField!
    @IBOutlet weak var txtFieldMonthlyAccounts: UITextField!
    @IBOutlet weak var txtFieldOutstandingLoans: UITextField!
    @IBOutlet weak var txtFieldSourceLoans: UITextField!
    @IBOutlet weak var txtFieldLoanAmount: UITextField!
    @IBOutlet weak var txtFieldHouseHold: UITextField!
    @IBOutlet weak var txtFieldHouseRent: UITextField!
    @IBOutlet weak var txtFieldUtilityBills: UITextField!
    @IBOutlet weak var txtFieldChildrenEducation: UITextField!
    @IBOutlet weak var txtFieldMedical: UITextField!
    @IBOutlet weak var txtFieldCommittee: UITextField!
    @IBOutlet weak var txtFieldLoanInstallments: UITextField!
    @IBOutlet weak var txtFieldMisc: UITextField!
    @IBOutlet weak var txtFieldTotalExpenses: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionAverageMonthly(_ sender: UIButton) {
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let monthlyTurnOver = self.txtFieldMonthlyTurnOver.text!
        let monthlyBusinessExpense = self.txtFieldMonthlyBusinessExpense.text!
        let otherIncome = self.txtFieldOtherIncome.text!
        let monthlyAccounts = self.txtFieldMonthlyAccounts.text!
        let outstandingLoan1 = self.txtFieldOutstandingLoans.text!
        let sourceofLoan1 = self.txtFieldSourceLoans.text!
        let outstandingLoan2 = self.txtFieldOutstandingLoans.text!
        let sourceofLoan2 = self.txtFieldSourceLoans.text!
        let loanAmount2 = self.txtFieldLoanAmount.text!
        let houseHold1 = self.txtFieldHouseHold.text!
        let houserent1 = self.txtFieldHouseRent.text!
        let utilitybills1 = self.txtFieldUtilityBills.text!
        let childeducation1 = self.txtFieldChildrenEducation.text!
        let medical1 = self.txtFieldMedical.text!
        let committee1 = self.txtFieldCommittee.text!
        let loaninstallment1 = self.txtFieldLoanInstallments.text!
        let other3 = self.txtFieldMisc.text!
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "averagemonthbusinessexp":monthlyBusinessExpense , "avgmonthturnover":monthlyTurnOver , "monthlyincome":monthlyTurnOver , "otherincome":otherIncome , "monthlyaccounts":monthlyAccounts , "outstandingloan1":outstandingLoan1 , "sourceofloan1":sourceofLoan1 , "outstandingloan2":outstandingLoan2 , "sourceofloan2":sourceofLoan2 , "loanamount2":loanAmount2 , "household1":houseHold1 , "houserent1":houserent1 , "childeducation1":childeducation1 , "medical1":medical1 , "committee1":committee1 , "loaninstallment1":loaninstallment1 , "other3":other3]
        print(dataDict)
        
        PlanVisitServices.BusinessFinancialInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            self.showBanner(title: "Success !!!", subTitle: "Your financial information form is submitted successfully" , type: SUCCESS)
            
            let vc = GeneralInformationVC.instantiateFromStoryboard()
            vc.isFromBusiness = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        })

    }

}

extension FinancialInformationViewController {
    
    func initialSetup() {
        viewSourceLoan.layoutIfNeeded()
        viewShadowInfo(view: viewSourceLoan, radius: 0.15)
        viewInnerSourceLoan.addShadowOnly(cornerRadius: viewInnerSourceLoan.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewLoanAmount.layoutIfNeeded()
        viewShadowInfo(view: viewLoanAmount, radius: 0.15)
        viewInnerLoanAmount.addShadowOnly(cornerRadius: viewInnerLoanAmount.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewAverageMonthly.layoutIfNeeded()
        viewShadowInfo(view: viewAverageMonthly, radius: 0.1)
        viewExpenseDetails.layoutIfNeeded()
        viewShadowInfo(view: viewExpenseDetails, radius: 0.01)
        viewHouseRent.layoutIfNeeded()
        viewShadowInfo(view: viewHouseRent, radius: 0.01)
        viewChildrenEducation.layoutIfNeeded()
        viewShadowInfo(view: viewChildrenEducation, radius: 0.01)
        viewCommittee.layoutIfNeeded()
        viewShadowInfo(view: viewCommittee, radius: 0.01)
        viewMisc.layoutIfNeeded()
        viewShadowInfo(view: viewMisc, radius: 0.01)
        viewTotalExpenses.layoutIfNeeded()
        viewShadowInfo(view: viewTotalExpenses, radius: 0.01)
        buttonDropShadow(button: buttonNext)
        Financial.setAttributedString(mainString: "Financial Information", coloredString: "Information")
        
    }
}
