//
//  FinancialInfoVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class SalariedFinancialInfoVC : UIViewController {
    
    class func instantiateFromStoryboard() -> SalariedFinancialInfoVC {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SalariedFinancialInfoVC
    }
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewTotalExpenses: UIView!
    @IBOutlet weak var viewMisc: UIView!
    @IBOutlet weak var viewCommittee: UIView!
    @IBOutlet weak var viewChildrenEducation: UIView!
    @IBOutlet weak var viewHouseRent: UIView!
    @IBOutlet weak var viewExpensesDetails: UIView!
    @IBOutlet weak var viewAverage: UIView!
    @IBOutlet weak var viewOutstanding: UIView!
    @IBOutlet weak var viewDo: UIView!
    @IBOutlet weak var viewMiscellaneous: UIView!
    @IBOutlet weak var viewExisting: UIView!
    @IBOutlet weak var viewSourceOfOtherIncome: UIView!
    @IBOutlet weak var viewOtherIncome: UIView!
    @IBOutlet weak var viewNetSalary: UIView!
    @IBOutlet weak var viewCurrentGrossSalary: UIView!
    @IBOutlet weak var Financial: UILabel!
    
    @IBOutlet weak var txtCurrentSalary: UITextField!
    @IBOutlet weak var txtNetSalary: UITextField!
    @IBOutlet weak var txtMonthlyIncome: UITextField!
    @IBOutlet weak var txtSourceOtherIncome: UITextField!
    @IBOutlet weak var txtExistingLoanInstallment: UITextField!
    @IBOutlet weak var txtMiscellaneous: UITextField!
    @IBOutlet weak var txtOutstandingLoans: UITextField!
    @IBOutlet weak var txtOutstandingLoanAmount: UITextField!
    
    @IBOutlet weak var txtHouseHold: UITextField!
    @IBOutlet weak var txtHouseRent: UITextField!
    @IBOutlet weak var txtUtilityBills: UITextField!
    @IBOutlet weak var txtChildrenEdu: UITextField!
    @IBOutlet weak var txtMedical: UITextField!
    @IBOutlet weak var txtCommitte: UITextField!
    @IBOutlet weak var txtLoanInstallment: UITextField!
    @IBOutlet weak var txtMisc: UITextField!
    @IBOutlet weak var txtTotalExpense: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let currentSalary = self.txtCurrentSalary.text!
        let netSalary = self.txtNetSalary.text!
        var monthlyIncome = self.txtMonthlyIncome.text!
        let otherIncome = self.txtSourceOtherIncome.text!
        let loanIncome = self.txtLoanInstallment.text!
        let miscellaneous = self.txtMiscellaneous.text!
        let outstandingLoan = self.txtOutstandingLoans.text!
        let outstandingLoanAmount = self.txtOutstandingLoanAmount.text!
        let houseHold = self.txtHouseHold.text!
        let houseRent = self.txtHouseRent.text!
        let utilityBills = self.txtUtilityBills.text!
        let childrenEdu = self.txtChildrenEdu.text!
        let medical = self.txtMedical.text!
        let committe = self.txtCommitte.text!
        let loanInstallment = self.txtLoanInstallment.text!
        let misc = self.txtMisc.text!
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "grosssalary":currentSalary , "netsalary":netSalary , "othersource":otherIncome , "natureofincome":loanIncome , "outstandingloan":outstandingLoan , "loanamount":outstandingLoanAmount , "household": houseHold , "utilitybill":utilityBills , "childeducation":childrenEdu , "medical":medical , "committe":committe , "loaninstallment":loanInstallment , "other0":misc]
        print(dataDict)
        
        PlanVisitServices.SalariedFinancialInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                
                self.showBanner(title: "Success !!!", subTitle: "Your financial information form is submitted successfully" , type: SUCCESS)
                
                let vc = GeneralInformationVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        })
    }
}

extension SalariedFinancialInfoVC {
    
    func initialSetup() {
        viewCurrentGrossSalary.layoutIfNeeded()
        viewShadowInfo(view: viewCurrentGrossSalary, radius: 0.1)
        viewNetSalary.layoutIfNeeded()
        viewShadowInfo(view: viewNetSalary, radius: 0.1)
        viewOtherIncome.layoutIfNeeded()
        viewShadowInfo(view: viewOtherIncome, radius: 0.1)
        viewSourceOfOtherIncome.layoutIfNeeded()
        viewShadowInfo(view: viewSourceOfOtherIncome, radius: 0.1)
        viewExisting.layoutIfNeeded()
        viewShadowInfo(view: viewExisting, radius: 0.1)
        viewMiscellaneous.layoutIfNeeded()
        viewShadowInfo(view: viewMiscellaneous, radius: 0.1)
        viewDo.layoutIfNeeded()
        viewShadowInfo(view: viewDo, radius: 0.1)
        viewOutstanding.layoutIfNeeded()
        viewShadowInfo(view: viewOutstanding, radius: 0.1)
        viewAverage.layoutIfNeeded()
        viewShadowInfo(view: viewAverage, radius: 0.1)
        viewExpensesDetails.layoutIfNeeded()
        viewShadowInfo(view: viewExpensesDetails, radius: 0.1)
        viewHouseRent.layoutIfNeeded()
        viewShadowInfo(view: viewHouseRent, radius: 0.1)
        viewChildrenEducation.layoutIfNeeded()
        viewShadowInfo(view: viewChildrenEducation, radius: 0.1)
        viewCommittee.layoutIfNeeded()
        viewShadowInfo(view: viewCommittee, radius: 0.1)
        viewMisc.layoutIfNeeded()
        viewShadowInfo(view: viewMisc, radius: 0.1)
        viewTotalExpenses.layoutIfNeeded()
        viewShadowInfo(view: viewTotalExpenses, radius: 0.1)
        buttonDropShadow(button: buttonNext)
        Financial.setAttributedString(mainString: "Financial Information", coloredString: "Information")
    }
    
}
