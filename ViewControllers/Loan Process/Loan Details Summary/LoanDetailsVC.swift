//
//  LoanDetailsVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/16/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class LoanDetailsVC: UIViewController {

    
    
    
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewInnerSPTwo: UIView!
    @IBOutlet weak var viewSPTwo: UIView!
    @IBOutlet weak var viewInnerSPOne: UIView!
    @IBOutlet weak var viewSPOne: UIView!
    @IBOutlet weak var viewInnerMobile: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewInnerLoan: UIView!
    @IBOutlet weak var viewLoan: UIView!
    @IBOutlet weak var Loan: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialSetup()
    }
    

    @IBAction func actionNext(_ sender: UIButton) {
        
        let vc = LoanStatusViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
extension LoanDetailsVC
{
    func initialSetup()
    {
        viewLoan.layoutIfNeeded()
        viewShadowInfo(view: viewLoan, radius: 0.15)
        viewInnerLoan.addShadowOnly(cornerRadius: viewInnerLoan.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewMobile.layoutIfNeeded()
        viewShadowInfo(view: viewMobile, radius: 0.15)
        viewInnerMobile.addShadowOnly(cornerRadius: viewInnerMobile.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewSPOne.layoutIfNeeded()
        viewShadowInfo(view: viewSPOne, radius: 0.15)
        viewInnerSPOne.addShadowOnly(cornerRadius: viewInnerSPOne.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewSPTwo.layoutIfNeeded()
        viewShadowInfo(view: viewSPTwo, radius: 0.15)
        viewInnerSPTwo.addShadowOnly(cornerRadius: viewInnerSPTwo.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewDate.layoutIfNeeded()
        viewShadowInfo(view: viewDate, radius: 0.1)
        buttonDropShadow(button: buttonNext)
        Loan.setAttributedString(mainString: "Loan Details Summary", coloredString: "Summary")
        
    }
}
