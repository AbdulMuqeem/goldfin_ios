//
//  PersonalInformationVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/12/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit
import GoogleMaps

extension PersonalInformationViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 12)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
        self.ShowAlert(title: "Access Permission", message: "Goldfin App needs to access your location to show your current location") { (sender) in
                    
            let url = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }

        }
        
    }
    
}


class PersonalInformationViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> PersonalInformationViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PersonalInformationViewController
    }
    
    var isDateSelected = true
    
    @IBOutlet weak var Personal: UILabel!
    @IBOutlet weak var viewInnerAddress: UIView!
    @IBOutlet weak var viewInnerFather: UIView!
    @IBOutlet weak var viewInnerCity: UIView!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var buttonCalculator: UIButton!
    @IBOutlet weak var txtFieldLoanAmount: UITextField!
    @IBOutlet weak var viewLoanAmount: UIView!
    @IBOutlet weak var labelEducation: UILabel!
    @IBOutlet weak var viewEducation: UIView!
    @IBOutlet weak var labelMaritalStatus: UILabel!
    @IBOutlet weak var viewMaritalStatus: UIView!
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var viewMale: UIView!
    @IBOutlet weak var txtFieldLivingYears: UITextField!
    @IBOutlet weak var viewLivingYears: UIView!
    @IBOutlet weak var labelResidenceStatus: UILabel!
    @IBOutlet weak var viewResidenceStatus: UIView!
    @IBOutlet weak var txtViewAddress: UITextView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var txtFieldPurposeOFLoan: UITextField!
    @IBOutlet weak var txtFatherName: UITextField!
    @IBOutlet weak var viewFatherName: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewPurposeOfLoan: UIView!
    @IBOutlet weak var mapView:GMSMapView!
    
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    var employementType:String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.labelDOB.text = self.getCurrentDate()
        self.mapView.delegate = self
        
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
        
//        self.view.contentInsetAdjustmentBehavior = .never
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPurpose(_ sender: UIButton) {
        showPickerView(tagPicker: 1)
    }
    
    @IBAction func actionSelectCity(_ sender: UIButton) {
        showPickerView(tagPicker: 2)
    }
    
    @IBAction func actionSelectResidence(_ sender: UIButton) {
        showPickerView(tagPicker: 3)
    }
    
    @IBAction func actionSelectGender(_ sender: UIButton) {
        showPickerView(tagPicker: 4)
    }
    
    @IBAction func actionSelectMarital(_ sender: UIButton) {
        showPickerView(tagPicker: 5)
    }
    
    @IBAction func actionSelectEducation(_ sender: UIButton) {
        showPickerView(tagPicker: 6)
    }
    
    func salariedInfo() {
        
        self.view.endEditing(true)
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let loanPurpose = self.txtFieldPurposeOFLoan.text!
        var city = self.labelCity.text!
        let fatherName = self.txtFatherName.text!
        var currentAddress = self.txtViewAddress.text!
        var residanceStatus = self.labelResidenceStatus.text!
        let livingYears = self.txtFieldLivingYears.text!
        let gender = self.labelGender.text!
        let maritalStatus = self.labelMaritalStatus.text!
        var education = self.labelEducation.text!
        let loan = self.txtFieldLoanAmount.text!
        let date = self.labelDOB.text!
        
        if city == "City:" {
            city = ""
        }
        
        if currentAddress == "Current Address:" {
            currentAddress = ""
        }
        
        if residanceStatus == "Residence Status?" {
            residanceStatus = ""
        }
        
        if education == "Education:" {
            education = ""
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "purpose":loanPurpose , "city":city , "fname":fatherName , "dob":date , "currentaddress":currentAddress , "residentialstatus":residanceStatus , "residingsince":"Birth" , "years":livingYears , "permanentaddress":currentAddress , "othermobilenum":Singleton.sharedInstance.CurrentUser!.phone! , "landline":Singleton.sharedInstance.CurrentUser!.phone! , "gender":gender , "maritalstatus":maritalStatus , "education":education , "loanAmount":loan]
        print(dataDict)
        
        PlanVisitServices.SalariedPersonalInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            self.showBanner(title: "Success !!!", subTitle: "Your personal information form is submitted successfully" , type: SUCCESS)
            
            let vc = SalariedProfessionalInformationVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        })

    }
    
    func businessInfo() {
        
        self.view.endEditing(true)
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let loanPurpose = self.txtFieldPurposeOFLoan.text!
        var city = self.labelCity.text!
        let fatherName = self.txtFatherName.text!
        var currentAddress = self.txtViewAddress.text!
        var residanceStatus = self.labelResidenceStatus.text!
        let livingYears = self.txtFieldLivingYears.text!
        let gender = self.labelGender.text!
        let maritalStatus = self.labelMaritalStatus.text!
        var education = self.labelEducation.text!
        let loan = self.txtFieldLoanAmount.text!
        let date = self.labelDOB.text!
        
        if city == "City:" {
            city = ""
        }
        
        if currentAddress == "Current Address:" {
            currentAddress = ""
        }
        
        if residanceStatus == "Residence Status?" {
            residanceStatus = ""
        }
        
        if education == "Education:" {
            education = ""
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["_id":id , "purpose":loanPurpose , "city":city , "fname":fatherName , "dob":date , "currentaddress":currentAddress , "residentialstatus":residanceStatus , "residingsince":"Birth" , "years":livingYears , "permanentaddress":currentAddress , "othermobilenum":Singleton.sharedInstance.CurrentUser!.phone! , "landline":Singleton.sharedInstance.CurrentUser!.phone! , "gender":gender , "maritalstatus":maritalStatus , "education":education , "loanAmount":loan]
        print(dataDict)
        
        PlanVisitServices.BusinessPersonalInformation(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            self.showBanner(title: "Success !!!", subTitle: "Your personal information form is submitted successfully" , type: SUCCESS)
            
            let vc = BussinessInformationVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        })

    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        if self.employementType == "Salaried" {
            self.salariedInfo()
        }
        else {
            self.businessInfo()
        }
    }
    
    @IBAction func actionCalculator(_ sender: UIButton) {
        let vc = LoanCalculatorViewController.instantiateFromStoryboard()
        vc.isFromPersonal = true
        self.present(vc, animated:true, completion: nil)
    }
    
    @IBAction func actionSelectDOB(_ sender: UIButton) {
        let popOverVC = UIStoryboard(name: "LoanProcess", bundle: nil).instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        popOverVC.delegate = self
        popOverVC.isFromPersonalInfo = true
        popOverVC.isDateFormat = isDateSelected
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }
    
}

extension PersonalInformationViewController {
    
    func initialSetup() {
        viewCity.layoutIfNeeded()
        viewInnerCity.layoutIfNeeded()
        viewShadowInfo(view: viewCity, radius: 0.15)
        viewInnerCity.addShadowOnly(cornerRadius: viewInnerCity.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewPurposeOfLoan.layoutIfNeeded()
        viewShadowInfo(view: viewPurposeOfLoan, radius: 0.15)
        viewFatherName.layoutIfNeeded()
        viewShadowInfo(view: viewFatherName, radius: 0.15)
        viewInnerFather.layoutIfNeeded()
        viewInnerFather.addShadowOnly(cornerRadius: viewInnerFather.frame.width * 0.15, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewDate.layoutIfNeeded()
        viewShadowInfo(view: viewDate, radius: 0.15)
        viewAddress.layoutIfNeeded()
        viewShadowInfo(view: viewAddress, radius: 0.1)
        viewInnerAddress.layoutIfNeeded()
        viewInnerAddress.addShadowOnly(cornerRadius: viewInnerAddress.frame.width * 0.1, maskedCorners: [.layerMinXMaxYCorner,.layerMinXMinYCorner], color: .clear, offset: CGSize(width: 0,height: 0), opacity: 0.0, shadowRadius: 0)
        viewResidenceStatus.layoutIfNeeded()
        viewShadowInfo(view: viewResidenceStatus, radius: 0.15)
        viewLivingYears.layoutIfNeeded()
        viewShadowInfo(view: viewLivingYears, radius: 0.15)
        viewMale.layoutIfNeeded()
        viewShadowInfo(view: viewMale, radius: 0.2)
        viewMaritalStatus.layoutIfNeeded()
        viewShadowInfo(view: viewMaritalStatus, radius: 0.2)
        viewEducation.layoutIfNeeded()
        viewShadowInfo(view: viewEducation, radius: 0.15)
        viewLoanAmount.layoutIfNeeded()
        viewShadowInfo(view: viewLoanAmount, radius: 0.15)
        
        buttonDropShadow(button: buttonSubmit)
        buttonDropShadow(button: buttonCalculator)
        Personal.setAttributedString(mainString: "Personal Information", coloredString: "Information")
    }
    
    func showPickerView(tagPicker : Int) {
        let popOverVC = UIStoryboard(name: "PopUp", bundle: nil).instantiateViewController(withIdentifier: "PurposeOfLoanPopUp") as! PurposeOfLoanPopUp
        popOverVC.delegate = self
        popOverVC.delegateEducation = self
        popOverVC.delegateMarital = self
        popOverVC.delegateGender = self
        popOverVC.delegateResidence = self
        popOverVC.delegateCity = self
        popOverVC.pickerTag = tagPicker
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle = .crossDissolve
        self.present(popOverVC, animated: false)
    }
    
}

extension PersonalInformationViewController : selectPurposeDelegate {
    
    func selectPurpose(purpose : String) {
        txtFieldPurposeOFLoan.text = purpose
    }
}

extension PersonalInformationViewController : selectCityDelegate {
    func selectCity(city: String) {
        labelCity.text = city
    }
}

extension PersonalInformationViewController : selectResidenceDelegate {
    func selectResidence(Residence: String) {
        labelResidenceStatus.text = Residence
    }
}

extension PersonalInformationViewController : selectGenderDelegate {
    func selectGender(gender: String) {
        labelGender.text = gender
    }
}

extension PersonalInformationViewController : selectMaritalDelegate {
    func selectMarital(marital: String) {
        labelResidenceStatus.text = marital
    }
}

extension PersonalInformationViewController : selectEducationDelegate {
    func selectEducation(education: String) {
        labelEducation.text = education
    }
}

extension PersonalInformationViewController : DateAndTimePickerDelegate {
    
    func sendTimeAndDate(DateAndTime: String) {
        print(DateAndTime)
        if isDateSelected == true {
            labelDOB.text = DateAndTime
        }
    }
    
}
