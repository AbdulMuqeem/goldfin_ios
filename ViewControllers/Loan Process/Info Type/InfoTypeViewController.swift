//
//  InfoTypeVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/12/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class InfoTypeViewController : UIViewController {
    
    class func instantiateFromStoryboard() -> InfoTypeViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InfoTypeViewController
    }
    
    @IBOutlet weak var viewFinancial: UIView!
    @IBOutlet weak var viewProfessional: UIView!
    @IBOutlet weak var viewPersonal: UIView!
    @IBOutlet weak var lblFinancial: UILabel!
    @IBOutlet weak var Loan: UILabel!
    
    var employementType:String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.CheckFormStatus()
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
    }
    
    @IBAction func actionPersonal(_ sender: UIButton) {
        
        if FORM_STATUS?.personalInformation == "true" {
            self.ShowAlert(title: "Alert", message: "You have already filled personal information, If you want to update details please click on ok") { (sender) in
                let vc = PersonalInformationViewController.instantiateFromStoryboard()
                if self.employementType == "Salaried" {
                    vc.employementType = "Salaried"
                }
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
        }
        
        let vc = PersonalInformationViewController.instantiateFromStoryboard()
        if self.employementType == "Salaried" {
            vc.employementType = "Salaried"
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func actionProfessional(_ sender: UIButton) {
        
        if FORM_STATUS?.personalInformation == "false" {
            self.showBanner(title: "Alert", subTitle: "Please fill personal information first", type: FAILURE)
            return
        }
        
        if self.employementType != "Salaried" {
            if FORM_STATUS?.professionalInformation == "true" {
                self.ShowAlert(title: "Alert", message: "You have already filled professional information, If you want to update details please click on ok") { (sender) in
                    let vc = BussinessInformationVC.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
            }
            
            let vc = BussinessInformationVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        if FORM_STATUS?.professionalInformation == "true" {
            self.ShowAlert(title: "Alert", message: "You have already filled professional information, If you want to update details please click on ok") { (sender) in
                let vc = SalariedProfessionalInformationVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
        }
        
        let vc = SalariedProfessionalInformationVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func actionFinancial(_ sender: UIButton) {
        
        if FORM_STATUS?.professionalInformation == "false" {
            self.showBanner(title: "Alert", subTitle: "Please fill professional information first", type: FAILURE)
            return
        }
        
        if self.employementType != "Salaried" {
            if FORM_STATUS?.financialInformation == "true" {
                self.ShowAlert(title: "Alert", message: "You have already filled financial information, If you want to update details please click on ok") { (sender) in
                    let vc = FinancialInformationViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
            }
            
            let vc = FinancialInformationViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        if FORM_STATUS?.financialInformation == "true" {
            self.ShowAlert(title: "Alert", message: "You have already filled financial information, If you want to update details please click on ok") { (sender) in
                let vc = SalariedFinancialInfoVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
        }
        
        let vc = SalariedFinancialInfoVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension InfoTypeViewController {
    
    func initialSetup() {
        viewDropShadow(view: viewPersonal)
        viewDropShadow(view: viewFinancial)
        viewDropShadow(view: viewProfessional)
        Loan.setAttributedString(mainString: "Loan Application Form (LAF)", coloredString: "(LAF)")
    }
}
