//
//  BusinessDocumentUploadViewController.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 19/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit
import ALCameraViewController
import Alamofire
import SwiftyJSON

extension BusinessDocumentUploadViewController : AlertViewDelegateAction {
    
    func okButtonAction() {
        let vc = LoanStatusViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func cancelAction() {
        let vc = LoanStatusViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

class BusinessDocumentUploadViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> BusinessDocumentUploadViewController {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! BusinessDocumentUploadViewController
    }
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonSkip: UIButton!
    @IBOutlet weak var viewFrontCNIC: UIView!
    @IBOutlet weak var viewBackCNIC: UIView!
    @IBOutlet weak var viewResidenceUtilityBills: UIView!
    @IBOutlet weak var viewBusinessUtilityBills: UIView!
    @IBOutlet weak var viewProofBusiness: UIView!
    @IBOutlet weak var viewProofBusinessIncome: UIView!
    @IBOutlet weak var viewSECPRegistration: UIView!
    @IBOutlet weak var viewNTN: UIView!
    @IBOutlet weak var Document: UILabel!
    
    @IBOutlet weak var frontCNICButton: UIButton!
    @IBOutlet weak var backCNICButton: UIButton!
    @IBOutlet weak var residenceUtilityBillsButton: UIButton!
    @IBOutlet weak var businessUtilityBillsButton: UIButton!
    @IBOutlet weak var proofOfBusinessButton: UIButton!
    @IBOutlet weak var proofOfBusinessIncomeButton: UIButton!
    @IBOutlet weak var secpRegistrationButton: UIButton!
    @IBOutlet weak var NTNButton: UIButton!
    
    @IBOutlet weak var frontCNICProgress: UIProgressView!
    @IBOutlet weak var backCNICProgress: UIProgressView!
    @IBOutlet weak var residenceUtilityBillsProgress: UIProgressView!
    @IBOutlet weak var businessUtilityBillsProgress: UIProgressView!
    @IBOutlet weak var proofOfBusinessProgress: UIProgressView!
    @IBOutlet weak var proofOfBusinessIncomeProgress: UIProgressView!
    @IBOutlet weak var secpRegistrationProgress: UIProgressView!
    @IBOutlet weak var NTNProgress: UIProgressView!
    
    @IBOutlet weak var alertView:AlertView!
    
    var cnicFront:UIImage!
    var cnicBack:UIImage!
    var residenceUtilityBills: UIImage!
    var businessUtilityBills: UIImage!
    var proofOfBusiness: UIImage!
    var proofOfBusinessIncome: UIImage!
    var secp: UIImage!
    var NTN:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        self.alertView.isHidden = true
        self.alertView.delegateAction = self
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSkip(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "AssesmentFormVC") as! AssesmentFormVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        DispatchQueue.main.async {
            
            var imageData1:Data!
            var imageData2:Data!
            var imageData3:Data!
            var imageData4:Data!
            var imageData5:Data!
            var imageData6:Data!
            var imageData7:Data!
            var imageData8:Data!
            
            let id = Singleton.sharedInstance.CurrentUser!.id!.data(using: .utf8)
            
            let apiURL = ServiceApiEndPoints.businessUploadDocuments
            print("URL : \(apiURL)")
            
            if self.cnicFront != nil {
                imageData1 = self.cnicFront.jpegData(compressionQuality: 0.5)!
            }
            
            if self.cnicBack != nil {
                imageData2 = self.cnicBack.jpegData(compressionQuality: 0.5)!
            }
            
            if self.residenceUtilityBills != nil {
                imageData3 = self.residenceUtilityBills.jpegData(compressionQuality: 0.5)!
            }
            
            if self.businessUtilityBills != nil {
                imageData4 = self.businessUtilityBills.jpegData(compressionQuality: 0.5)!
            }
            
            if self.proofOfBusiness != nil {
                imageData5 = self.proofOfBusiness.jpegData(compressionQuality: 0.5)!
            }
            
            if self.proofOfBusinessIncome != nil {
                imageData6 = self.proofOfBusinessIncome.jpegData(compressionQuality: 0.5)!
            }
            
            if self.secp != nil {
                imageData7 = self.secp.jpegData(compressionQuality: 0.5)!
            }
            
            if self.NTN != nil {
                imageData8 = self.NTN.jpegData(compressionQuality: 0.5)!
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(id!, withName: "_id")
                if imageData1 != nil {
                    multipartFormData.append(imageData1, withName: "cnicFront", fileName: "cnicFront.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData2 != nil {
                    multipartFormData.append(imageData2, withName: "cnicBack", fileName: "cnicBack.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData3 != nil {
                    multipartFormData.append(imageData3, withName: "residenceUtilityBills", fileName: "residenceUtilityBills.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData4 != nil {
                    multipartFormData.append(imageData4, withName: "residenceBusinessBills", fileName: "residenceBusinessBills.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData5 != nil {
                    multipartFormData.append(imageData5, withName: "proofOfBusiness", fileName: "proofOfBusiness.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData6 != nil {
                    multipartFormData.append(imageData5, withName: "proofOfBusinessIncome", fileName: "proofOfBusinessIncome.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData7 != nil {
                    multipartFormData.append(imageData5, withName: "SECP", fileName: "SECP.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData8 != nil {
                    multipartFormData.append(imageData5, withName: "NTN", fileName: "NTN.jpeg" ,mimeType: "image/jpeg")
                }
                
            }, to:apiURL)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("progress::\(progress)")
                    })
                    upload.responseData(completionHandler: { response in
                        
                        if response.result.isSuccess {
                            
                            if let value: Any = response.result.value as AnyObject? {
                                
                                self.stopLoading()
                                let response = JSON(value)
                                print("Response: \(response)")
                                
                            }
                        }
                        else {
                            self.stopLoading()
                            print("FAILURE")
                            
                            let error = "\(String(describing: (response.error?.localizedDescription)!))"
                            self.showBanner(title: "Alert", subTitle: error , type: FAILURE)
                        }
                    })
                    
                case .failure(let encodingError):
                    
                    self.stopLoading()
                    
                    let error = "\(encodingError.localizedDescription))"
                    self.showBanner(title: "Alert", subTitle: error , type: FAILURE)
                    break
                }
            }
            
        }
        
        self.alertView.isHidden = false
        
    }
    
}

extension BusinessDocumentUploadViewController {
    
    func initialSetup() {
        viewFrontCNIC.layoutIfNeeded()
        viewShadowInfo(view: viewFrontCNIC, radius: 0.1)
        viewBackCNIC.layoutIfNeeded()
        viewShadowInfo(view: viewBackCNIC, radius: 0.1)
        viewResidenceUtilityBills.layoutIfNeeded()
        viewShadowInfo(view: viewResidenceUtilityBills, radius: 0.1)
        viewBusinessUtilityBills.layoutIfNeeded()
        viewShadowInfo(view: viewBusinessUtilityBills, radius: 0.1)
        viewProofBusiness.layoutIfNeeded()
        viewShadowInfo(view: viewProofBusiness, radius: 0.1)
        viewProofBusinessIncome.layoutIfNeeded()
        viewShadowInfo(view: viewProofBusinessIncome, radius: 0.1)
        viewSECPRegistration.layoutIfNeeded()
        viewShadowInfo(view: viewSECPRegistration, radius: 0.1)
        viewNTN.layoutIfNeeded()
        viewShadowInfo(view: viewNTN, radius: 0.1)
        buttonDropShadow(button: buttonNext)
        buttonDropShadow(button: buttonSkip)
        Document.setAttributedString(mainString: "Documents Uploading", coloredString: "Uploading")
    }
    
}

extension BusinessDocumentUploadViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func FrontCNICAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "CNIC_Front")
    }
    
    @IBAction func BackCNICAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "CNIC_Back")
    }
    
    @IBAction func residenceUtilityBillAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Residence Utility Bills")
    }
    
    @IBAction func businessUtilityAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Business Utility Bills")
    }
    
    @IBAction func proofOfBusinessAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Proof of Business")
    }
    
    @IBAction func proofOfBusinessIncomeAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Proof of Business Income")
    }
    
    @IBAction func secpAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "SECP")
    }
    
    @IBAction func NTNAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "NTN")
    }
    
    func openCamera(id:String){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            if let pickedImage = image {
                
                if id == "CNIC_Front" {
                    self!.cnicFront = pickedImage
                    self!.frontCNICProgress.progress = 1.0
                }
                else if id == "CNIC_Back" {
                    self!.cnicBack = pickedImage
                    self!.backCNICProgress.progress = 1.0
                }
                else if id == "Residence Utility Bills" {
                    self!.residenceUtilityBills = pickedImage
                    self!.residenceUtilityBillsProgress.progress = 1.0
                }
                else if id == "Business Utility Bills" {
                    self!.businessUtilityBills = pickedImage
                    self!.businessUtilityBillsProgress.progress = 1.0
                }
                else if id == "Proof of Business" {
                    self!.proofOfBusiness = pickedImage
                    self!.proofOfBusinessProgress.progress = 1.0
                }
                else if id == "Proof of Business Income" {
                    self!.proofOfBusinessIncome = pickedImage
                    self!.proofOfBusinessIncomeProgress.progress = 1.0
                }
                else if id == "SECP" {
                    self!.secp = pickedImage
                    self!.secpRegistrationProgress.progress = 1.0
                }
                else if id == "NTN" {
                    self!.NTN = pickedImage
                    self!.NTNProgress.progress = 1.0
                }
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
}
