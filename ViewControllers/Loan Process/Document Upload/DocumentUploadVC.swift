//
//  DocumentUploadVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/15/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit
import ALCameraViewController
import Alamofire
import SwiftyJSON

extension DocumentUploadVC : AlertViewDelegateAction {
    
    func okButtonAction() {
        let vc = LoanStatusViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func cancelAction() {
        let vc = LoanStatusViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

class DocumentUploadVC: UIViewController {
    
    class func instantiateFromStoryboard() -> DocumentUploadVC {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DocumentUploadVC
    }
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonSkip: UIButton!
    @IBOutlet weak var viewCopies: UIView!
    @IBOutlet weak var viewProof: UIView!
    @IBOutlet weak var viewVisiting: UIView!
    @IBOutlet weak var viewCopy: UIView!
    @IBOutlet weak var viewCNIC: UIView!
    @IBOutlet weak var Document: UILabel!
    
    @IBOutlet weak var frontCNICButton: UIButton!
    @IBOutlet weak var backCNICButton: UIButton!
    @IBOutlet weak var utilityBillsButton: UIButton!
    @IBOutlet weak var currentSalaryButton: UIButton!
    @IBOutlet weak var certificateButton: UIButton!
    
    @IBOutlet weak var frontCNICProgress: UIProgressView!
    @IBOutlet weak var backCNICProgress: UIProgressView!
    @IBOutlet weak var utilityProgress: UIProgressView!
    @IBOutlet weak var currentSalaryProgress: UIProgressView!
    @IBOutlet weak var jobCertificateProgress: UIProgressView!
    
    @IBOutlet weak var alertView:AlertView!
    
    var cnicFront:UIImage!
    var cnicBack:UIImage!
    var utilityBills:UIImage!
    var currentSalary:UIImage!
    var jobCertificate:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        self.alertView.isHidden = true
        self.alertView.delegateAction = self
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSkip(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "AssesmentFormVC") as! AssesmentFormVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        DispatchQueue.main.async {
            
            var imageData1:Data!
            var imageData2:Data!
            var imageData3:Data!
            var imageData4:Data!
            var imageData5:Data!
            
            let id = Singleton.sharedInstance.CurrentUser!.id!.data(using: .utf8)
            
            let apiURL = ServiceApiEndPoints.salariedUploadDocuments
            print("URL : \(apiURL)")
            
            if self.cnicFront != nil {
                imageData1 = self.cnicFront.jpegData(compressionQuality: 0.5)!
            }
            
            if self.cnicBack != nil {
                imageData2 = self.cnicBack.jpegData(compressionQuality: 0.5)!
            }
            
            if self.utilityBills != nil {
                imageData3 = self.utilityBills.jpegData(compressionQuality: 0.5)!
            }
            
            if self.currentSalary != nil {
                imageData4 = self.currentSalary.jpegData(compressionQuality: 0.5)!
            }
            
            if self.jobCertificate != nil {
                imageData5 = self.jobCertificate.jpegData(compressionQuality: 0.5)!
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(id!, withName: "_id")
                if imageData1 != nil {
                    multipartFormData.append(imageData1, withName: "cnicFront", fileName: "cnicFront.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData2 != nil {
                    multipartFormData.append(imageData2, withName: "cnicBack", fileName: "cnicBack.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData3 != nil {
                    multipartFormData.append(imageData3, withName: "residenceUtilityBills", fileName: "residenceUtilityBills.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData4 != nil {
                    multipartFormData.append(imageData4, withName: "currentSalarySlip", fileName: "currentSalarySlip.jpeg" ,mimeType: "image/jpeg")
                }
                if imageData5 != nil {
                    multipartFormData.append(imageData5, withName: "jobCertificate", fileName: "jobCertificate.jpeg" ,mimeType: "image/jpeg")
                    
                }
                
            }, to:apiURL)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("progress::\(progress)")
                    })
                    upload.responseData(completionHandler: { response in
                        
                        if response.result.isSuccess {
                            
                            if let value: Any = response.result.value as AnyObject? {
                                
                                self.stopLoading()
                                let response = JSON(value)
                                print("Response: \(response)")
                                
                            }
                        }
                        else {
                            self.stopLoading()
                            print("FAILURE")
                            
                            let error = "\(String(describing: (response.error?.localizedDescription)!))"
                            self.showBanner(title: "Alert", subTitle: error , type: FAILURE)
                        }
                    })
                    
                case .failure(let encodingError):
                    
                    self.stopLoading()
                    
                    let error = "\(encodingError.localizedDescription))"
                    self.showBanner(title: "Alert", subTitle: error , type: FAILURE)
                    break
                }
            }
            
        }
        
        self.alertView.isHidden = false
        
    }
    
}

extension DocumentUploadVC {
    
    func initialSetup() {
        viewCNIC.layoutIfNeeded()
        viewShadowInfo(view: viewCNIC, radius: 0.1)
        viewCopy.layoutIfNeeded()
        viewShadowInfo(view: viewCopy, radius: 0.1)
        viewVisiting.layoutIfNeeded()
        viewShadowInfo(view: viewVisiting, radius: 0.1)
        viewProof.layoutIfNeeded()
        viewShadowInfo(view: viewProof, radius: 0.1)
        viewCopies.layoutIfNeeded()
        viewShadowInfo(view: viewCopies, radius: 0.1)
        buttonDropShadow(button: buttonNext)
        buttonDropShadow(button: buttonSkip)
        Document.setAttributedString(mainString: "Documents Uploading", coloredString: "Uploading")
    }
    
}

extension DocumentUploadVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func FrontCNICAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "CNIC_Front")
    }
    
    @IBAction func BackCNICAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "CNIC_Back")
    }
    
    @IBAction func utilityBillAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Utility Bills")
    }
    
    @IBAction func currentSalaryAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Salary")
    }
    
    @IBAction func jobCertificateAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: "Job")
    }
    
    func openCamera(id:String){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            if let pickedImage = image {
                
                if id == "CNIC_Front" {
                    self!.cnicFront = pickedImage
                    self!.frontCNICProgress.progress = 1.0
                }
                else if id == "CNIC_Back" {
                    self!.cnicBack = pickedImage
                    self!.backCNICProgress.progress = 1.0
                }
                else if id == "Utility Bills" {
                    self!.utilityBills = pickedImage
                    self!.utilityProgress.progress = 1.0
                }
                else if id == "Salary" {
                    self!.currentSalary = pickedImage
                    self!.currentSalaryProgress.progress = 1.0
                }
                else if id == "Job" {
                    self!.jobCertificate = pickedImage
                    self!.jobCertificateProgress.progress = 1.0
                }
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
}
