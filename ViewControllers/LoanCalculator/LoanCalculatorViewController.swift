//
//  LoanCalculatorViewController.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 02/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit
import DropDown

extension LoanCalculatorViewController : UITextFieldDelegate {
    
    func initialSetup() {
        addShadow(view: viewCalculate)
        addShadow(view: viewPkr)
        addShadow(view: viewTola)
        Goldfin.setAttributedString(mainString: "GoldFin Loan Calculator", coloredString: "Calculator")
        buttonDropShadow(button: buttonApplyNow)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.txtFieldGold.text = ""
            self.txtFieldGold.isUserInteractionEnabled = false
            self.apiType = "amount"
            return true
        }
        
        if textField.tag == 2 {
            self.txtFieldAmount.text = ""
            self.txtFieldAmount.isUserInteractionEnabled = false
            return true
        }
        
        return true
    }
    
}

class LoanCalculatorViewController : UIViewController {
    
    class func instantiateFromStoryboard() -> LoanCalculatorViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoanCalculatorViewController
    }
    
    @IBOutlet weak var buttonApplyNow: UIButton!
    @IBOutlet weak var buttonCalculate: UIButton!
    @IBOutlet weak var labelCalculate: UILabel!
    @IBOutlet weak var viewCalculate: UIView!
    @IBOutlet weak var viewTola: UIView!
    @IBOutlet weak var txtFieldGold: UITextField!
    @IBOutlet weak var viewPkr: UIView!
    @IBOutlet weak var txtFieldAmount: UITextField!
    @IBOutlet weak var Goldfin: UILabel!
    
    @IBOutlet weak var buttonGold: UIButton!
    @IBOutlet weak var lblGoldTitle: UILabel!
    
    var apiType:String = ""
    var goldDropDown = DropDown()
    var goldArray:[String] = ["Gram" , "Tola"]
    
    var isFromPersonal:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtFieldAmount.delegate = self
        self.txtFieldGold.delegate = self
        
        self.initialSetup()
        self.hideKeyboardWhenTappedAround()
        
        // Get Gold DropDown
        goldDropDown.anchorView = buttonGold // UIView or UIBarButtonItem
        goldDropDown.bottomOffset = CGPoint(x: 0 , y: (goldDropDown.anchorView?.plainView.bounds.height)!)
        goldDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblGoldTitle.text = item
            self.apiType = item
            
            if self.txtFieldAmount.text!.isEmptyOrWhitespace() == false {
                self.amountCalculate()
            }
        }
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if self.isFromPersonal == true {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
    }
    
    
    @IBAction func actionGold(_ sender: UIButton) {
        self.view.endEditing(true)
        self.goldDropDown.dataSource = self.goldArray
        self.goldDropDown.show()
    }
    
    @IBAction func actionCurrency(_ sender: UIButton) {
        
    }
    
    @IBAction func actionApplyNow(_ sender: UIButton) {
        if self.isFromPersonal == true {
            self.dismiss(animated: true, completion: nil)
            return
        }
        let vc = RegisterViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionCalculate(_ sender: UIButton) {
        
        if self.apiType == "amount" {
            self.amountCalculate()
        }
        else if self.lblGoldTitle.text == "Tola" {
            self.tolaCalculate()
        }
        else if self.lblGoldTitle.text == "Gram" {
            self.gramCalculate()
        }
        else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid amount OR valid gold unit", type: FAILURE)
        }
    }
    
    func amountCalculate() {
        
        self.view.endEditing(true)
        
        let amount = self.txtFieldAmount.text!
        
        if amount.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter valid amount", type: FAILURE)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        AMOUNT = amount
        
        UserServices.CalculateAmount(completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                if self.lblGoldTitle.text == "Tola" {
                    if let value = response["tolaRequired"].string {
                        self.txtFieldGold.text = value.removeLetters().removingWhitespaces()
                        self.txtFieldGold.isUserInteractionEnabled = true
                        self.txtFieldAmount.isUserInteractionEnabled = true
                    }
                }
                else {
                    if let value = response["gramRequired"].string {
                        self.txtFieldGold.text = value.removeLetters().removingWhitespaces()
                        self.txtFieldGold.isUserInteractionEnabled = true
                        self.txtFieldAmount.isUserInteractionEnabled = true
                    }
                }
            }
            
        })
        
    }
    
    func tolaCalculate() {
        
        self.view.endEditing(true)
        
        let tola = self.txtFieldGold.text!
        
        if tola.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter valid value for gold", type: FAILURE)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        TOLA = tola.removeLetters()
        self.txtFieldGold.text = TOLA
        
        UserServices.CalculateTola(completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                if let value = response["requiredAmount"].double {
                    self.txtFieldAmount.text = "\(value)"
                    self.txtFieldGold.isUserInteractionEnabled = true
                    self.txtFieldAmount.isUserInteractionEnabled = true
                }
            }
            
        })
        
    }
    
    func gramCalculate() {
        
        self.view.endEditing(true)
        
        let gram = self.txtFieldGold.text!
        
        if gram.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter valid weight of gold", type: FAILURE)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        GRAM = gram.removeLetters()
        self.txtFieldGold.text = GRAM
        
        UserServices.CalculateGram(completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                if let value = response["requiredAmount"].double {
                    self.txtFieldAmount.text = "\(value)"
                    self.txtFieldGold.isUserInteractionEnabled = true
                    self.txtFieldAmount.isUserInteractionEnabled = true
                }
            }
            
        })
        
    }
    
}


