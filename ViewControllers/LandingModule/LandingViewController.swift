//
//  InterestVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 02/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension LandingViewController {
    
    func initialSetup(){
        
        if self.isFromAppDelegate == true {
            self.backButtonView.isHidden = true
        }
        
        viewDropShadow(view: viewRegister)
        viewDropShadow(view: viewCalculate)
        labelThankYou.setAttributedString(mainString: "Thank you for your interest in MSQ", coloredString: "MSQ")
        buttonFAQ.underline()
        viewBullet.layoutIfNeeded()
        bulletViews(view: viewBullet)
        
    }
    
}

class LandingViewController : UIViewController {
    
    class func instantiateFromStoryboard() -> LandingViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LandingViewController
    }
    
    @IBOutlet weak var viewBullet: UIView!
    @IBOutlet weak var buttonFAQ: UIButton!
    @IBOutlet weak var labelThankYou: UILabel!
    @IBOutlet weak var viewCalculate: UIView!
    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak var backButtonView: UIView!
    
    var isFromAppDelegate:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.HideNavigationBar()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        let vc = LoanCalculatorViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func actionCalculate(_ sender: UIButton) {
        let vc = LoanCalculatorViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionRegister(_ sender: UIButton) {
        let vc = RegisterViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionFAQ(_ sender: UIButton) {
        let vc = FAQViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

