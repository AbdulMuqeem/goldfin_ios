//
//  FAQViewController.swift
//  GoldFin
//
//  Created by Muqeem's Macbook on 28/11/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension FAQViewController {
    
    
}

class FAQViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> FAQViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FAQViewController
    }
    
    @IBOutlet weak var lblFAQ: UILabel!
    
    @IBOutlet weak var FirstTitleView:UIView!
    @IBOutlet weak var FirstImageBackgroundColor:UIView!
    @IBOutlet weak var FirstImage:UIImageView!
    @IBOutlet weak var FirstDescriptionView:UIView!
    @IBOutlet weak var lblFirstTitle:UILabel!
    @IBOutlet weak var lblFirstDescription:UILabel!
    
    @IBOutlet weak var SecondTitleView:UIView!
    @IBOutlet weak var SecondImageBackgroundColor:UIView!
    @IBOutlet weak var SecondImage:UIImageView!
    @IBOutlet weak var SecondDescriptionView:UIView!
    @IBOutlet weak var lblSecondTitle:UILabel!
    @IBOutlet weak var lblSecondDescription:UILabel!
    
    var showFirstView = true
    var showSecondView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblFAQ.setAttributedString(mainString: "FAQ's", coloredString: "s")
        self.initData()
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initData() {
        
        self.lblFirstDescription.text = "“MeraSarmayaQarza” (MSQ) is a secured loan against the pledge of gold as collateral. The value of the loan is about 80% of the present value (market price) of the net weight of the gold pledged. It can be availed for working capital, business expansion, agriculture and livestock, domestic reasons, family emergencies and other lawful purposes."
        
        self.lblSecondDescription.text = "Gold such as; bars, jewelry and ornaments, can be pledged as collateral. The value of the loan will depend on the purity of the gold and on present value of its net weight equal to 24 karats. The value of gems, stones, lace and string will not be considered and only the value of the net weight of gold will be calculated."
        
        
    }
    
    @IBAction func firstAction(_ sender : UIButton) {
        
        if showFirstView == true {
            self.FirstTitleView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.FirstTitleView.BorderColor = UIColor.init(rgb: THEME_COLOR)
            self.FirstImageBackgroundColor.backgroundColor = .white
            self.FirstImage.image = UIImage(named: "minus")
            self.lblFirstTitle.textColor = .white
            self.FirstDescriptionView.isHidden = false
        }
        else {
            self.FirstTitleView.backgroundColor = .white
            self.FirstTitleView.BorderColor = .white
            self.FirstImageBackgroundColor.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.FirstImage.image = UIImage(named: "plus")
            self.lblFirstTitle.textColor = .black
            self.FirstDescriptionView.isHidden = true
        }
        
        showFirstView = !showFirstView
    
    }
    
    @IBAction func secondAction(_ sender : UIButton) {
        
        if showSecondView == true {
            self.SecondTitleView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.SecondTitleView.BorderColor = UIColor.init(rgb: THEME_COLOR)
            self.SecondImageBackgroundColor.backgroundColor = .white
            self.SecondImage.image = UIImage(named: "minus")
            self.lblSecondTitle.textColor = .white
            self.SecondDescriptionView.isHidden = false
        }
        else {
            self.SecondTitleView.backgroundColor = .white
            self.SecondTitleView.BorderColor = .white
            self.SecondImageBackgroundColor.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.SecondImage.image = UIImage(named: "plus")
            self.lblSecondTitle.textColor = .black
            self.SecondDescriptionView.isHidden = true
        }
        
        showSecondView = !showSecondView
    
    }

}

