//
//  PurposeOfLoanPopUp.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/23/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

protocol selectPurposeDelegate {
    func selectPurpose(purpose : String)
}

protocol selectCityDelegate {
    func selectCity(city : String)
}

protocol selectResidenceDelegate {
    func selectResidence(Residence : String)
}

protocol selectGenderDelegate {
    func selectGender(gender : String)
}

protocol selectMaritalDelegate {
    func selectMarital(marital : String)
}

protocol selectEducationDelegate {
    func selectEducation(education : String)
}

protocol selectProfessionDelegate {
    func selectProfession(Profession : String)
}

protocol jobStatusDelegate {
    func selectJobStatus(status : String)
}

class PurposeOfLoanPopUp: UIViewController {
    
    var arrPurpose : [String] = ["Working Capital", "Business Enhancement", "Asset Purchase", "Repair And Maintenance", "Agriculture", "Livestock", "Domestic Needs", "Emergency" , "Other"]
    var arrCity : [String] = ["Karachi", "Lahore", "Islamabad"]
    var arrResidence : [String] = ["Owned", "Family", "Rented"]
    var arrGender : [String] = ["Male", "Female"]
    var arrMarital : [String] = ["Single", "Married"]
    var arrEducation : [String] = ["Matric", "Intermediate","Bachelors","Masters"]
    
    var arrProfession : [String] = ["Salaried", "Business"]
    var arrJob : [String] = ["Intern", "Contract" , "Permanent"]
    
    var delegate : selectPurposeDelegate!
    var delegateCity : selectCityDelegate!
    var delegateResidence : selectResidenceDelegate!
    var delegateGender : selectGenderDelegate!
    var delegateMarital : selectMaritalDelegate!
    var delegateEducation : selectEducationDelegate!
    
    var delegateProfession : selectProfessionDelegate!
    var delegateJobStatus : jobStatusDelegate!
    
    @IBOutlet weak var pickerViewPurpose: UIPickerView!
    @IBOutlet weak var pickerViewCity: UIPickerView!
    @IBOutlet weak var pickerViewResidence: UIPickerView!
    
    @IBOutlet weak var pickerViewEducation: UIPickerView!
    @IBOutlet weak var pickerViewGender: UIPickerView!
    @IBOutlet weak var pickerViewMarital: UIPickerView!
    
    @IBOutlet weak var pickerViewProfession: UIPickerView!
    @IBOutlet weak var pickerViewJobStatus: UIPickerView!
    
    var selectValue : String!
    var pickerTag : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if pickerTag == 1 {
            pickerViewPurpose.isHidden = false
            pickerViewCity.isHidden = true
            pickerViewResidence.isHidden = true
            pickerViewGender.isHidden = true
            pickerViewMarital.isHidden = true
            pickerViewEducation.isHidden = true
            pickerViewProfession.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrPurpose[0]
        }
        else if pickerTag == 2 {
            pickerViewCity.isHidden = false
            pickerViewPurpose.isHidden = true
            pickerViewResidence.isHidden = true
            pickerViewGender.isHidden = true
            pickerViewMarital.isHidden = true
            pickerViewEducation.isHidden = true
            pickerViewProfession.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrCity[0]
        }
        else if pickerTag == 3 {
            pickerViewResidence.isHidden = false
            pickerViewCity.isHidden = true
            pickerViewPurpose.isHidden = true
            pickerViewGender.isHidden = true
            pickerViewMarital.isHidden = true
            pickerViewEducation.isHidden = true
            pickerViewProfession.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrResidence[0]
        }
        else if pickerTag == 4 {
            pickerViewGender.isHidden = false
            pickerViewResidence.isHidden = true
            pickerViewCity.isHidden = true
            pickerViewPurpose.isHidden = true
            pickerViewMarital.isHidden = true
            pickerViewEducation.isHidden = true
            pickerViewProfession.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrGender [0]
        }
        else if pickerTag == 5 {
            pickerViewMarital.isHidden = false
            pickerViewGender.isHidden = true
            pickerViewResidence.isHidden = true
            pickerViewCity.isHidden = true
            pickerViewPurpose.isHidden = true
            pickerViewEducation.isHidden = true
            pickerViewProfession.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrMarital[0]
        }
        else if pickerTag == 6 {
            pickerViewEducation.isHidden = false
            pickerViewMarital.isHidden = true
            pickerViewGender.isHidden = true
            pickerViewResidence.isHidden = true
            pickerViewCity.isHidden = true
            pickerViewPurpose.isHidden = true
            pickerViewProfession.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrEducation[0]
        }
        else if pickerTag == 7 {
            pickerViewProfession.isHidden = false
            pickerViewEducation.isHidden = true
            pickerViewMarital.isHidden = true
            pickerViewGender.isHidden = true
            pickerViewResidence.isHidden = true
            pickerViewCity.isHidden = true
            pickerViewPurpose.isHidden = true
            pickerViewJobStatus.isHidden = true
            selectValue = arrProfession[0]
        }
        else if pickerTag == 8 {
            pickerViewJobStatus.isHidden = false
            pickerViewEducation.isHidden = true
            pickerViewMarital.isHidden = true
            pickerViewGender.isHidden = true
            pickerViewResidence.isHidden = true
            pickerViewCity.isHidden = true
            pickerViewPurpose.isHidden = true
            pickerViewProfession.isHidden = true
            selectValue = arrJob[0]
        }
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
        
        if pickerTag == 1 {
            delegate.selectPurpose(purpose: selectValue)
        }
            
        else if pickerTag == 2 {
            delegateCity.selectCity(city: selectValue)
        }
            
        else if pickerTag == 3 {
            delegateResidence.selectResidence(Residence: selectValue)
        }
            
        else if pickerTag == 4 {
            delegateGender.selectGender(gender: selectValue)
        }
            
        else if pickerTag == 5 {
            delegateMarital.selectMarital(marital: selectValue)
        }
            
        else if pickerTag == 6 {
            delegateEducation.selectEducation(education: selectValue)
        }
            
        else if pickerTag == 7 {
            delegateProfession.selectProfession(Profession: selectValue)
        }
            
        else if pickerTag == 8 {
            delegateJobStatus.selectJobStatus(status:selectValue) 
        }
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension PurposeOfLoanPopUp : UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        var count = 0
        
        if pickerView.tag == 1 {
            count = arrPurpose.count
        }
            
        else if pickerView.tag == 2 {
            count = arrCity.count
        }
            
        else if pickerView.tag == 3 {
            count = arrResidence.count
        }
            
        else if pickerView.tag == 4 {
            count = arrGender.count
        }
            
        else if pickerView.tag == 5 {
            count = arrMarital.count
        }
            
        else if pickerView.tag == 6 {
            count = arrEducation.count
        }
            
        else if pickerView.tag == 7 {
            count = arrProfession.count
        }
            
        else if pickerView.tag == 8 {
            count = arrJob.count
        }
        
        return count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var title = ""
        
        if pickerView.tag == 1 {
            title = arrPurpose[row]
        }
            
        else if pickerView.tag == 2 {
            title = arrCity[row]
        }
            
        else if pickerView.tag == 3 {
            title = arrResidence[row]
        }
            
        else if pickerView.tag == 4 {
            title = arrGender[row]
        }
            
        else if pickerView.tag == 5 {
            title = arrMarital[row]
        }
            
        else if pickerView.tag == 6 {
            title = arrEducation[row]
        }
            
        else if pickerView.tag == 7 {
            title = arrProfession[row]
        }
            
        else if pickerView.tag == 8 {
            title = arrJob[row]
        }
        
        return title
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            selectValue = arrPurpose[row]
        }
            
        else if pickerView.tag == 2 {
            selectValue = arrCity[row]
        }
            
        else if pickerView.tag == 3 {
            selectValue = arrResidence[row]
        }
            
        else if pickerView.tag == 4 {
            selectValue = arrGender[row]
        }
            
        else if pickerView.tag == 5 {
            selectValue = arrMarital[row]
        }
            
        else if pickerView.tag == 6 {
            selectValue = arrEducation[row]
        }
            
        else if pickerView.tag == 7 {
            selectValue = arrProfession[row]
        }
            
        else if pickerView.tag == 8 {
            selectValue = arrJob[row]
        }
        
    }
    
}

