//
//  SignInVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/11/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {
    
    @IBOutlet weak var buttonViewSignUp: UIButton!
    @IBOutlet weak var buttonViewSignIn: UIButton!
    @IBOutlet weak var buttonBiometricLogin: UIButton!
    @IBOutlet weak var buttonSignIn: UIButton!
    @IBOutlet weak var buttonShowPassword: UIButton!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var txtFieldUsername: UITextField!
    @IBOutlet weak var viewSignInContainer: UIView!
    @IBOutlet weak var viewSignUp: UIView!
    @IBOutlet weak var viewSignIn: UIView!
    
    var delegate:SignInDelegate!
    var showPassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    
    @IBAction func actionShowPassword(_ sender: UIButton) {
        
        if showPassword == true {
            self.buttonShowPassword.setImage(UIImage(named: "hide"), for: .normal)
            self.txtFieldPassword.isSecureTextEntry = false
        } else {
            self.buttonShowPassword.setImage(UIImage(named: "hide"), for: .normal)
            self.txtFieldPassword.isSecureTextEntry = true
        }
        
        showPassword = !showPassword
    }
    
    @IBAction func actionSignIn(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let userName = self.txtFieldUsername.text!
        let password = self.txtFieldPassword.text!
        
        if userName.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter valid User Name / CNIC", type: FAILURE)
            return
        }
        
        if password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter valid password", type: FAILURE)
            return
        }

        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["userName":userName , "cnic":userName , "password":password]
        print(dataDict)
        
        UserServices.Login(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                Singleton.sharedInstance.CurrentUser = nil
                
                let userResultObj = User(object:response)
                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                
                self.showBanner(title: "Success!!!", subTitle: "You're login successfully" , type: SUCCESS)
                
                if let type = Singleton.sharedInstance.CurrentUser!.type {
                    if type == "customer" {
                        if let form = Singleton.sharedInstance.CurrentUser!.formDone {
                            if form == "false" {
                                let vc = PlanVisitViewController.instantiateFromStoryboard()
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else {
                                let vc = LoanStatusViewController.instantiateFromStoryboard()
                                vc.isFromSignIn = true
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else {
                            let vc = PlanVisitViewController.instantiateFromStoryboard()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                
            }
            
        })
    }

    
    @IBAction func actionForget(_ sender: UIButton) {
        self.showBanner(title: "Alert", subTitle: "Will be implemented", type:FAILURE)
    }
    
    @IBAction func actionBiometricLogin(_ sender: UIButton) {
        self.showBanner(title: "Alert", subTitle: "Will be implemented", type:FAILURE)
    }
    
    @IBAction func actionViewSignIn(_ sender: UIButton) {
        delegate.showSignInView()
        viewSignIn.addshadow(top: true, left: true, bottom: false, right: false)
        viewSignUp.addshadow(top: false, left: false, bottom: false, right: false)
    }
    
    @IBAction func actionviewSignUp(_ sender: UIButton) {
        delegate.showSignInUpView()
        viewSignUp.addshadow(top: true, left: false, bottom: false, right: true)
        viewSignIn.addshadow(top: false, left: false, bottom: false, right: false)
    }
    
}

extension SignInVC {
    
    func initialSetup() {
//        viewSignInContainer.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 1.0)
    }
    
}
