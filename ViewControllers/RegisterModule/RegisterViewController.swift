//
//  AuthVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/11/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

protocol SignInDelegate {
    func showSignInView()
    func showSignInUpView()
}

protocol SignUpDelegate {
    func showSignUpView()
    func showSignUpInView()
}

class RegisterViewController : UIViewController {
    
    class func instantiateFromStoryboard() -> RegisterViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RegisterViewController
    }
    
    @IBOutlet weak var containerSignIn: UIView!
    @IBOutlet weak var containerSignUp: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.containerSignIn.isHidden = false
        self.containerSignUp.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        if SIGNUP == true {
            SIGNUP = false
            self.containerSignUp.isHidden = true
            self.containerSignIn.isHidden = false
        }
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SignUpContainer") {
            let embedVC = segue.destination as! SignUpVC
            embedVC.delegate = self
        }
        if (segue.identifier == "SignInContainer") {
            let embedVC = segue.destination as! SignInVC
            embedVC.delegate = self
        }
    }
    
}

extension RegisterViewController : SignInDelegate {
    
    func showSignInView() {
        containerSignIn.isHidden = false
        containerSignUp.isHidden = true
    }
    func showSignInUpView() {
        containerSignUp.isHidden = false
        containerSignIn.isHidden = true
    }
}

extension RegisterViewController : SignUpDelegate {
    
    func showSignUpView() {
        containerSignUp.isHidden = false
        containerSignIn.isHidden = true
    }
    
    func showSignUpInView() {
        containerSignUp.isHidden = true
        containerSignIn.isHidden = false
    }
    
}
