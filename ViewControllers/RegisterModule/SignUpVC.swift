//
//  SignUpVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 8/11/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension SignUpVC :UITextFieldDelegate {
    
    func initialSetup() {
//        viewSignUpContainer.addshadow(top: false, left: true, bottom: true, right: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 1 {
            
            if textField.text!.count == 5 || textField.text!.count == 13 {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let code = strcmp(char, "\\b")
                    if code == -92 {
                        return true
                    }
                    else {
                        textField.text!.append("-")
                        return true
                    }
                }
            }
            
            if textField.text!.count >= 15 {
                
                if let char = string.cString(using: String.Encoding.utf8) {
                    let code = strcmp(char, "\\b")
                    if code == -92 {
                        return true
                    }
                    else {
                        return false
                    }
                }
                            
            }
            
        }
        
        if textField.tag == 2 {
            
            if textField.text == "+92-" {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let code = strcmp(char, "\\b")
                    if code == -92 {
                        return false
                    }
                    else {
                        return true
                    }
                }
            }
            
            if textField.text!.count == 7 {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let code = strcmp(char, "\\b")
                    if code == -92 {
                        return true
                    }
                    else {
                        textField.text!.append("-")
                        return true
                    }
                }
            }
            
            if textField.text!.count >= 15 {
                
                if let char = string.cString(using: String.Encoding.utf8) {
                    let code = strcmp(char, "\\b")
                    if code == -92 {
                        return true
                    }
                    else {
                        return false
                    }
                }
            }
            
        }
        
        return true
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if textField.tag == 2 {
            textField.text = "+92-"
        }

        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if textField == self.txtFieldName {
            self.txtFieldCNIC.becomeFirstResponder()
        }
        
        if textField == self.txtFieldCNIC {
            self.txtFieldNumber.becomeFirstResponder()
        }
        
        if textField == self.txtFieldNumber {
            self.txtFieldEmail.becomeFirstResponder()
        }
        
        if textField == self.txtFieldEmail {
            self.txtFieldUserName.becomeFirstResponder()
        }
        
        if textField == self.txtFieldUserName {
            self.txtFieldPassword.becomeFirstResponder()
        }
        
        if textField == self.txtFieldPassword {
            self.signUp()
        }
        
        return true
        
    }
    
    
}

class SignUpVC: UIViewController {
    
    
    @IBOutlet weak var SignUp: UILabel!
    @IBOutlet weak var SignIn: UILabel!
    @IBOutlet weak var buttonSignUp: UIButton!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldNumber: UITextField!
    @IBOutlet weak var txtFieldCNIC: UITextField!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var viewSignUp: UIView!
    @IBOutlet weak var viewSignIn: UIView!
    
    @IBOutlet weak var buttonShowPassword: UIButton!
    @IBOutlet weak var viewSignUpContainer: UIView!
    
    var delegate : SignUpDelegate!
    var showPassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        self.txtFieldName.delegate = self
        self.txtFieldCNIC.delegate = self
        self.txtFieldNumber.delegate = self
        self.txtFieldEmail.delegate = self
        self.txtFieldEmail.delegate = self
        self.txtFieldUserName.delegate = self
        self.txtFieldPassword.delegate = self
        
//        self.txtFieldCNIC.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
//        self.txtFieldNumber.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
    }
    
    @IBAction func actionViewSignIn(_ sender: UIButton) {
        delegate.showSignUpInView()
        viewSignIn.addshadow(top: true, left: true, bottom: false, right: false)
        viewSignUp.addshadow(top: false, left: false, bottom: false, right: false)
    }
    
    @IBAction func actionViewSignUp(_ sender: UIButton) {
        delegate.showSignUpView()
        viewSignUp.addshadow(top: true, left: false, bottom: false, right: true)
        viewSignIn.addshadow(top: false, left: false, bottom: false, right: false)
    }
    
    @IBAction func actionSignUp(_ sender: UIButton) {
        self.signUp()
    }
    
    func signUp() {
        self.view.endEditing(true)
        
        let fullName = self.txtFieldName.text!
        let cnic = self.txtFieldCNIC.text!
        let number = self.txtFieldNumber.text!
        let userName = self.txtFieldUserName.text!
        let password = self.txtFieldPassword.text!
        
        if fullName.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter name", type: FAILURE)
            return
        }
        
        if !AppHelper.isValidName(testStr: cnic, minPhone: 15, maxPhone: 15) {
            self.showBanner(title: "Alert", subTitle: "Please enter valid CNIC number" , type:WARNING)
            return
        }
        
        if !AppHelper.isValidName(testStr: number, minPhone: 15, maxPhone: 15) {
            self.showBanner(title: "Alert", subTitle: "Please enter valid mobile number" , type:WARNING)
            return
        }
        
        guard let email = self.txtFieldEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , type:WARNING)
            return
        }
        
        if userName.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "plz enter user name", type: FAILURE)
            return
        }
        
        if password.count < 7 {
            self.showBanner(title: "Alert", subTitle: "Password must be greater than 7 characters, should contain at least one digit, special character & upper case character" , type:WARNING)
            return
        }
        
        if !AppHelper.isValidPassword(testStr: password) {
            self.showBanner(title: "Alert", subTitle: "Password must be greater than 7 characters, should contain at least one digit & upper case character" , type:WARNING)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        let dataDict:[String:Any] = ["email":email , "password":password ,  "userName":userName , "fullname":fullName , "cnic":cnic , "phone":number]
        print(dataDict)
        
        UserServices.Signup(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            if let response = response?["data"] {
                
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                Singleton.sharedInstance.CurrentUser = nil
                
                let userResultObj = User(object:response)
                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                
                self.showBanner(title: "Congratulations !!!", subTitle: "You're account has been created successfully" , type: SUCCESS)
                
                
                let vc = OTPViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
        })
        
    }
    
    @IBAction func actionShowPassword(_ sender: UIButton) {
        if showPassword == true {
            self.buttonShowPassword.setImage(UIImage(named: "hide"), for: .normal)
            self.txtFieldPassword.isSecureTextEntry = false
        } else {
            self.buttonShowPassword.setImage(UIImage(named: "hide"), for: .normal)
            self.txtFieldPassword.isSecureTextEntry = true
        }
        
        showPassword = !showPassword
    }
    
}

