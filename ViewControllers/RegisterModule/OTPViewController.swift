//
//  OTPViewController.swift
//  GoldFin
//
//  Created by Muqeem's Macbook on 22/11/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {

    class func instantiateFromStoryboard() -> OTPViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! OTPViewController
    }
    
    @IBOutlet weak var codeTextField:CodeTextField!
    @IBOutlet weak var lblTimer:UILabel!
    
    @IBOutlet weak var btnSubmitAction:UIButton!
    @IBOutlet weak var btnResendAction:UIButton!
    
    var counter = 59
    var mobileNumber:String = ""
    
    var isCallApi:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnResendAction.isHidden = true
        self.setupPinField()
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }



    @objc func updateCounter() {
        
        if self.counter == 0 {
            self.lblTimer.text = "00:00"
            self.btnResendAction.isHidden = false
            return
        }
        
        if self.counter > 0 {
            self.lblTimer.text = "00:\(counter)"
            counter -= 1
        }
        
    }

    func setupPinField() {
        
        self.codeTextField.textColor = UIColor.init(rgb: 0xFFC91A)
        self.codeTextField.placeholder = ""
        self.codeTextField.becomeFirstResponder()
        self.codeTextField.refreshUI()
        
        self.codeTextField.textChangeHandler = { text, completed in
            
            if let value = text {
                
                if value.count == 6 {
                    self.isCallApi = true
                }
                
            }
        }
    }

    
    @IBAction func submitAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if self.isCallApi == false {
            self.showBanner(title: "Alert", subTitle: "Please enter 6 digit OTP", type: FAILURE)
            return
        }
        
        let code = self.codeTextField.text!
        var userID = ""
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        if let id = Singleton.sharedInstance.CurrentUser!.id {
            userID = id
        }
        
        let dataDict:[String:Any] = ["verifyCode":code , "userId":userID]
        print(dataDict)
        
        UserServices.SendOTP(param: dataDict, completionHandler: {(status, response, error) in
            
            if !status {
                let msg = "Unable to process your request, please try again"
                if error != nil {
                    self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
                if response != nil {
                    if let msg = response?["Message"].string {
                        self.showBanner(title: "Error", subTitle: msg , type: FAILURE)
                        print(response!)
                        self.stopLoading()
                        return
                    }
                    self.showBanner(title: "Alert", subTitle: msg , type: FAILURE)
                    self.stopLoading()
                    return
                }
            }
            
            self.stopLoading()
            print(response!)
            
            
            self.showBanner(title: "Success!!!", subTitle: "Phone number has been verified", type: SUCCESS)
            let vc = ThankYouViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
                            
        })

    }

    @IBAction func resendOTPAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
//        self.startLoading(message: "")
        
        
        self.btnResendAction.isHidden = true
        self.counter = 59
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
    }
    
}
