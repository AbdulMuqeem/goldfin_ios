//
//  Welcome2ViewController.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 02/09/2020.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension Welcome2ViewController {
    
    func initialSetup() {
        fastAndEasy.setAttributedString(mainString: "Fast And Easy", coloredString: "")
        makeBulletView()
    }
    
    func makeBulletView(){
        viewBullet1.layoutIfNeeded()
        bulletViews(view: viewBullet1)
        viewBullet2.layoutIfNeeded()
        bulletViews(view: viewBullet2)
        viewBullet3.layoutIfNeeded()
        bulletViews(view: viewBullet3)
    }
    
}

class Welcome2ViewController : UIViewController {
    
    class func instantiateFromStoryboard() -> Welcome2ViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! Welcome2ViewController
    }
    
    @IBOutlet weak var viewBullet3: UIView!
    @IBOutlet weak var viewBullet2: UIView!
    @IBOutlet weak var viewBullet1: UIView!
    
    @IBOutlet weak var viewNext: ShadowView!
    @IBOutlet weak var moneyIn: UILabel!
    @IBOutlet weak var assessmentAt: UILabel!
    @IBOutlet weak var loanRequest: UILabel!
    @IBOutlet weak var threeEasySteps: UILabel!
    @IBOutlet weak var fastAndEasy: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        //Swipe Gesture
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.HideNavigationBar()
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .right {
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == .left {
            let vc = LandingViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        let vc = LandingViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


