//
//  SpecsVC.swift
//  GoldFin
//
//  Created by Abdul Muqeem on 7/25/20.
//  Copyright © 2020 GoldFin. All rights reserved.
//

import UIKit

extension WelcomeViewController {
    
    func initialSetup() {
        
        self.viewNext.layoutIfNeeded()
        theBestValue.setAttributedString(mainString: "The Best Value Of Mera Sarmaya Qarza(MSQ)", coloredString: "(MSQ)")
    }
    
}

class WelcomeViewController: UIViewController {

    class func instantiateFromStoryboard() -> WelcomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WelcomeViewController
    }
    
    @IBOutlet weak var viewNext: ShadowView!
    @IBOutlet weak var loanSettlement: UILabel!
    @IBOutlet weak var noPenalty: UILabel!
    @IBOutlet weak var noGuarantee: UILabel!
    @IBOutlet weak var oneYearLoan: UILabel!
    @IBOutlet weak var lowestMarkup: UILabel!
    @IBOutlet weak var theBestValue: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UserDefaults.standard.set("true", forKey: "isFirstLaunched")
        self.initialSetup()
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.HideNavigationBar()
    }

    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        let vc = Welcome2ViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        let vc = Welcome2ViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let storyboard = UIStoryboard(name: "LoanProcess", bundle: nil)
//        let nextVC = storyboard.instantiateViewController(withIdentifier: "PersonalInformationVC") as! PersonalInformationVC//StepsVC
//        self.navigationController?.pushViewController(nextVC, animated: true)
    }

}

